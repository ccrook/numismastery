module.exports = function(grunt) {
  grunt.initConfig({
    //pkg: grunt.file.readJSON('package.json'),
    // Default Configurations
    env: {
      dev: {
        NODE_ENV: 'development'
      },
      test: {
        NODE_ENV: 'test'
      }
    },

    nodemon: {
      dev: {
        script: 'server.js',
        options: {
          ext:'js, html',
          watch: ['server.js', 'config/**/*.js', 'server/**/*.js']
        }
      },
      debug: {
        script: 'server.js',
        options: {
          nodeArgs: ['--debug'],
          ext: 'js, html',
          watch: ['server.js', 'config/**/*.js', 'server/**/*.js']
        }
      }
    },

    watch: {
      js: {
        files: ['server.js', 'config/**/*.js', 'server/**/*.js', 'public/js/**/*.js'],
        tasks: ['jshint']
      },
      // Note: lines below used for browsify - will rebundle public js modules on change
      // src: {
      //   files: ['public/js/**/*.js', 'public/js/**/*.html'],
      //   tasks: ['browserify']
      // },
      css: {
        files: ['public/css/*.css'],
        tasks: ['csslint']
      }
    },

    concurrent: {
      dev: {
        tasks: ['nodemon', 'watch'],
        options: {
          logConcurrentOutput: true
        }
      },
      debug: {
        tasks: ['nodemon:debug', 'watch', 'node-inspector'],
        options: {
          logConcurrentOutput: true
        }
      },
      e2e: {
        tasks: ['concurrent:dev', 'protractor:firefox'],
        options: {
          logConcurrentOutput: true
        }
      }
    },

    // browserify: {
    //   main: {
    //     src: 'public/js/app.js',
    //     dest: 'public/main.js'
    //   },
    //   options: {
    //     transform: ['brfs']
    //   }
    // },

    'node-inspector': {
      debug: {
        options: {
          'web-port': 8989
        }
      }
    },

    // Testing Configurations
    mochaTest: {
      src: ['test/**/*.js', '!**/protractor/**'],
      options: {
        reporter: 'spec'
      }
    },


    // Linting Configurations
    jshint: {
      all: {
        src: ['server.js', 'config/**/*.js', 'server/**/*.js', 'public/js/*.js']
      },
      options: {
        laxcomma: true
      }
    },
    csslint: {
      all: {
        src: 'public/css/*.css'
      }
    },

    protractor: {
      options: {
        // Task-specific options go here.
        configFile: "test/protractor/conf.js",//"node_modules/protractor/example/conf.js",
        keepAlive: false,
        singleRun: true,
        output: "results.txt",
        
      },
      //Targets
      firefox: {
        args: {
          browser: 'firefox'
        }
      },
      chrome: {
        args: {
          browser: 'chrome'
        }
      },
    },
  });

  // Load Default Tasks
  grunt.loadNpmTasks('grunt-env');
  grunt.loadNpmTasks('grunt-nodemon');
  grunt.loadNpmTasks('grunt-concurrent');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-node-inspector');
  grunt.loadNpmTasks('grunt-protractor-runner');

  // Load Build Tasks
  // grunt.loadNpmTasks('grunt-browserify');

  // Load Testing Tasks
  grunt.loadNpmTasks('grunt-mocha-test');

  // Load Linting Tasks
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-csslint');

  // Register Tasks
  grunt.registerTask('default', ['env:dev', 'lint', 'concurrent:dev']);
  grunt.registerTask('debug', ['env:dev', 'lint', 'concurrent:debug']);
  grunt.registerTask('test', ['env:test', 'mochaTest']);
  grunt.registerTask('test:e2e', ['concurrent:e2e']);
  grunt.registerTask('lint', ['jshint', 'csslint']);
  //grunt.registerTask('protractor', ['env:test', 'protractorTest']);
};
