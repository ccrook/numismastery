# Numismastery

TODO:


*Users & Authentication*
* Add username to MongoDB model
* Force user to enter username when logging in (if they don't have one already, disallow email addresses & duplicates)
* Change row keys to hash of username+timestamp, so as not to divulge email addresses
* User Profile page
  * Uploaded coins w/ links to details (summary of current crowd grades? date uploaded? )
  * Change Password/User details/payment stuff
  * User stats overall (i.e. standard deviation of grades vs. pro - need to begin tracking)
  * Badges earned (achievements?)

*Coin Upload*
* Enforce minimum resolution
* Make PCGS number optional
* Validate series against year entered


*Grading Search*
* Allow user to select whether user-uploaded coins should be included, or just professional


*Grading*
* Update scale to include PR and + grades
* Add Problem Coin statuses
* Add Variety tagging (just via text for now? Unless we can rip variety per series & date)
* Add Image Tagging
  * Variety Pickup Points
  * Damage
  * Cleaning Evidence
* Add suffix tagging (aggregation of counts of each?) - i.e. DMPL, PL, DCAM, FB, FBL, etc.

*Monetization*
* Setup PayPal api
* Research how to limit grades unless paying subscription (filters? aggregating a grade count each month?)

*UI*
* Pretty up w/ logo and color scheme
* Select2s for dropdowns with autocompletes


*Data*
* Mark series with Proof grades (PFXX) with Prefix PROOF 
