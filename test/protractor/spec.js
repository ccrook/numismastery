var urlChanged = function(url) {
  return function () {
    return browser.getCurrentUrl().then(function(actualUrl) {
      return url != actualUrl;
    });
  };
};

describe('Splash page', function() {
  it('clicking Start Grading button takes user to homepage', function() {
    //browser.get('http://staging.numismastery.com');
    browser.get('http://localhost:3030');

    browser.getCurrentUrl().then(function(url){
      //expect(url).toEqual('http://staging.numismastery.com/')
      expect(url).toEqual('http://localhost:3030/');

      element(by.css('.btn-outlined')).click().then(function(){
        browser.getCurrentUrl().then(function(url){
          expect(url).toEqual('http://localhost:3030/home')
        })
      })
    });
    //element(by.model('todoText')).sendKeys('write a protractor test');



    // var todoList = element.all(by.repeater('todo in todos'));
    // expect(todoList.count()).toEqual(3);
    // expect(todoList.get(2).getText()).toEqual('write a protractor test');
  });
});

//TODO: cover registration
//TODO: cover login
