var server = require('../server.js');
var app = server.app;
var chai = require('chai');
var expect = chai.expect;

describe('Server bootstrapping tests:', function() {
  it('app reference should exist', function() {
    expect(app).to.exist;
    expect(app).to.be.a('function');
  });
});
