var server = require('../../server.js');
var app = server.app;
var chai = require('chai');
var expect = chai.expect;

//var config = require('../config');
// var mongoose = require('mongoose');
// require('../../server/models/User');
var User = require('mongoose').model('User');
var Coins = require('mongoose').model('Coins');
var CoinGrades = require('mongoose').model('CoinGrades');
var CoinComments = require('mongoose').model('CoinComments');
var GradeScale = require('mongoose').model('GradeScale');

var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;

var init = require('./ModelInitializer')();

describe('Coins Model: ', function() {

  //Stolen from: http://rob.conery.io/2012/02/25/testing-your-model-with-mocha-mongo-and-nodejs/
  //coins for use in each test
  var testCoins = null;

  beforeEach(function(done){
    //add some test data
    //console.log('before each');

    testCoins = init.setup(done);
  });

  afterEach(function(done){
    //console.log("after each");
    //delete all test coins
    testCoins = init.tearDown(done);
  });

  describe("Coins model bootstrapping tests: ", function(){
    it('Should exist', function() {
      expect(Coins).to.exist;
      expect(Coins).to.be.a('function');
    });

    it("Test should create test data", function(){
      Coins.findOne(testCoins.alreadyGradedCoin, function(err, coin){
        expect(coin).to.exist;
      });
    });
  })

  describe("Coins -> Grades Tests: ", function(){
    it("Should allow adding grade", function(){
      // var grade = new CoinGrades({
      //   //TODO: add data
      // });
      // notYetGradedCoin.addGrade(grade).then(funtion(err, updatedCoin){
      //   console.log("UPDATED COIN", updatedCoin);
      // });
    });

    it("Should allow removing grade", function(){

    });

    it("Should recalculate stats when a grade is added", function(){

    });

    it("Should recalculate stats when a grade is remove", function(){

    });

  });

  describe("Coins -> Comments Tests: ", function(){
    it("getComments() should return comments for coin", function(){
        var coinWithCommentsPopulated = Coins.findOne(testCoins.alreadyGradedCoin,function(err, coin){
          if(!err){
            coin.getComments(function(err, comments){
              expect(comments).to.be.an('array');
              expect(comments.length).equals(2);
              expect(comments[0].body).to.exist;
            });
          }
        })
    });

    it("addComment() should add a comment", function(){
      var coinWithCommentsPopulated = Coins.findOne(testCoins.alreadyGradedCoin,function(err, coin){
          coin.addComment(new CoinComments({
            body: "new comment"
          }), function(err, success){

            success.coin.getComments(function(err, comments){
              expect(comments.length).equals(3);

            });
          });
      });
    });

    it("deleteComment() should delete a comment", function(){
      Coins.findOne(testCoins.alreadyGradedCoin,function(err, coin){
         coin.getComments(function(err, existingComments){
           coin.deleteComment(existingComments[0], function(err, success){

             coin.getComments(function(err, comments){
               //Started with two, so deleting one should leave us with one
               expect(comments.length).equals(1);

             });
           });
         });
      });
    });
  });


  describe("Coins.getUngradedCoinInSeries() Tests: ", function(){
    it("Should return single ungraded coin of series", function(){
      Coins.getUngradedCoinInSeries(testCoins.morganSeries, {}, testCoins.testUser2, function(err, ungradedCoinsInSeries){
        expect(ungradedCoinsInSeries).to.exist;
        expect(ungradedCoinsInSeries.pcgs_number).equals('1');
      });
    });

    it("Should return null if no ungraded coins are found", function(){
      Coins.getUngradedCoinInSeries(testCoins.morganSeries, {}, testCoins.testUser1, function(err, ungradedCoin){
        // expect(ungradedCoin).to.exist;
        // expect(ungradedCoin).equals(null);
      });
    });

//TODO: not convinced this test is working as expected
    it("Should return a different coin after grading", function(){
      Coins.getUngradedCoinInSeries(testCoins.morganSeries, {}, testCoins.testUser2, function(err, ungradedCoin){
        ungradedCoin.addGrade(new CoinGrades({
          coin: ungradedCoin,
          gradedBy: testCoins.testUser2,
          grade: GradeScale.findOne({})
        }), function(err, response){
           console.log(response);
          Coins.getUngradedCoinInSeries(testCoins.morganSeries, {}, testCoins.testUser2, function(err, ungradedCoin2){
            // expect(err).to.not.exist;
            // expect(ungradedCoin2).to.exist;
            // expect(ungradedCoin2._id).to.exist;
            expect(ungradedCoin2._id).to.not.deep.equal(ungradedCoin._id);
            console.log(ungradedCoin2._id, ungradedCoin._id);
          });
        });


      })
    })
  });

  //
  describe("Coins.gradedByUserSince() tests:", function(){
    it("Should return Coins graded by user since past date", function(){
      Coins.gradedByUserSince(testCoins.testUser1, new Date("2/27/2016"), function(err, coinsGraded){
        expect(coinsGraded.length).equals(1);
      });
    });

    it("Should NOT return Coins graded by user since future date", function(){
      Coins.gradedByUserSince(testCoins.testUser1, new Date("2/28/2016"), function(err, coinsGraded){
        expect(coinsGraded).to.exist;
        expect(coinsGraded.length).equals(0);
      })
    });
  });




});
