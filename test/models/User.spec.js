var server = require('../../server.js');
var app = server.app;
var chai = require('chai');
var expect = chai.expect;

//CPC: Theres probably a cleaner way to require these
var User = require('mongoose').model('User');
var Coins = require('mongoose').model('Coins');
var CoinGrades = require('mongoose').model('CoinGrades');
var CoinComments = require('mongoose').model('CoinComments');
var GradeScale = require('mongoose').model('GradeScale');

var init = require('./ModelInitializer')();

describe('User model tests:', function() {
  var testHelper = null;

  beforeEach(function(done){
    //add some test data
    //console.log('before each');
    testHelper = init.setup(done);
  });

  afterEach(function(done){
    //console.log("after each");
    //delete all test coins
    testHelper = init.tearDown(done);
  });

  //Stolen from: http://rob.conery.io/2012/02/25/testing-your-model-with-mocha-mongo-and-nodejs/
  //holds a customer to use in the each test
  //var currentCustomer = null;

  // beforeEach(function(done){
  //   //add some test data
  //   customer.register("test@test.com", "password", "password", function(doc){
  //     currentCustomer = doc;
  //     done();
  //   });
  // });
  //
  // afterEach(function(done){
  //   //delete all the customer records
  //   customer.model.remove({}, function() {
  //     done();
  //   });
  // });

  it('User model should exist', function() {
    expect(User).to.exist;
    expect(User).to.be.a('function');
  });

  it("User has findOrCreate method", function(){
    expect(User.findOrCreate).to.exist;
    expect(User.findOrCreate).to.be.a('function');
  });

  describe("modelInstance.getGradedCoins() Tests: ", function(){
    it("Should return all coins graded by user", function(){
      testHelper.testUser1.getGradedCoins(function(err, gradedCoins){
        expect(gradedCoins).to.exist;
        expect(gradedCoins.length).equals(1);
      });
    })
  });

  describe("modelInstance.getComments() Tests: ", function(){
    it("Should return all comments posted by user", function(){
      testHelper.testUser1.getComments(function(err, allComments){
        expect(allComments).to.exist;
        expect(allComments.length).equals(1);
      });
    });

  });

  describe("modelInstance.getUploadedCoins() Tests: ", function(){
    it("Should return all coins uploaded by user", function(){
      testHelper.testUser1.getUploadedCoins(function(err, allUploadedCoins){
        expect(allUploadedCoins).to.exist;
        expect(allUploadedCoins.length).equals(2);
      });
    });

    it("Should return null if uuser hasn't uploaded any coins", function(){
      testHelper.testUser2.getUploadedCoins(function(err, allUploadedCoins){
        expect(allUploadedCoins).to.be.an('Array');
        expect(allUploadedCoins.length).equals(0);
      });
    });
  });

  describe("gradesSinceLastRecaptcha", function(){
    it("Should return count of grades since last re-captcha", function(){
      testHelper.testUser1.gradesSinceLastRecaptcha(function(err, gradeCount){
        expect(gradeCount).to.equal(1);
      })
    })
  })

});


//TODO: require Mongoose models

//TODO: write failing tests for functions off of model
