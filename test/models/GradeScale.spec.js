// var server = require('../../server.js');
// var app = server.app;
// var chai = require('chai');
// var expect = chai.expect;
//
// //var config = require('../config');
// // var mongoose = require('mongoose');
// // require('../../server/models/User');
// var Coins = require('mongoose').model('Coins');
// var CoinGrades = require('mongoose').model('CoinGrades');
// var mongoose = require('mongoose');
// var ObjectId = mongoose.Types.ObjectId;
//
// var init = require('./ModelInitializer')();
//
// describe('Coins Model: ', function() {
//
//   //Stolen from: http://rob.conery.io/2012/02/25/testing-your-model-with-mocha-mongo-and-nodejs/
//   //coins for use in each test
//   var testCoins = null;
//
//   beforeEach(function(done){
//     //add some test data
//     testCoins = init.setup(done);
//   });
//
//   afterEach(function(done){
//     //delete all test coins
//     init.tearDown(done);
//   });
//
//   it('Should exist', function() {
//     expect(Coins).to.exist;
//     expect(Coins).to.be.a('function');
//   });
//
//   it("Test should create test data", function(){
//     //console.log(testCoins);
//     console.log(mongoose.Types.ObjectId.isValid(testCoins.alreadyGradedCoin._id.toString()));
//     console.log(testCoins.alreadyGradedCoin._id, typeof(testCoins.alreadyGradedCoin._id), new ObjectId(testCoins.alreadyGradedCoin._id));
//     Coins.findOne({country: {$eq: "USA"}}, function(err, coin){
//       console.log(err, coin, testCoins.alreadyGradedCoin._id);
//       //TODO: CPC: this fails because findById isn't working
//       //expect(coin).to.exist;
//
//
//     });
//
//
//     //expect(CoinGrades.find({_id: testCoins.alreadyGradedCoin.grades[0]})).to.exist;
//   });
//
//   it("Should allow adding grade", function(){
//
//   });
//
//   it("Should allow adding comment", function(){
//
//   });
//
//   it("Should recalculate stats when a grade is added", function(){
//
//   });
//
//   it("Should find coins by User", function(){
//
//   });
//
//   it("Should find ungraded coin of series", function(){
//
//   });
//
//   it("Should throw an error if no ungraded coins are found", function(){
//
//   });
//
//   it("Should return Coins graded by user in last X minutes", function(){
//
//   });
//
//   it("Should allow getComments", function(){
//     //function(){
//     //console.log("COMMENTS START", testCoins.alreadyGradedCoin);
//       var coinWithCommentsPopulated = Coins.findOne(testCoins.alreadyGradedCoin,function(err, coin){
//         if(!err){
//           coin.getComments(function(err, comments){
//             console.log("GET COMMENTS", comments);
//             expect(comments).to.be.an('array');
//             expect(comments[0].body).equals('DERRRRP');
//           });
//         }
//       })
//   });
//
//   it("Should allow getGades", function(){
//     //console.log("GRADE START", testCoins.alreadyGradedCoin);
//       var coinWithCommentsPopulated = Coins.findOne(testCoins.alreadyGradedCoin).populate("grades").exec(function(err, coin){
//         console.log("GRADE CALLBACK", coin);
//       })
//   });
// });
