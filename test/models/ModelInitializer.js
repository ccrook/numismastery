// var server = require('../../server.js');
// var app = server.app;

//TODO: create and export a way to setup and teardown a test DB

//For reference:

// pcgs_number: String,
// cert_number: String,
// date_mintmark: String,
// denomination: String,
// country: String,
// grade: String,
// image_url: String,
// series: String,
// images: {
//   "obverse": [],
//   "reverse": [],
//   "edge": []
// },
// comments: [{type: mongoose.Schema.Types.ObjectId, ref: 'CoinComments'}],
// grades: [{type: mongoose.Schema.Types.ObjectId, ref: 'CoinGrades'}],
// stats: {
//   sumOfUserGrades: Number,
//   averageUserGradeValue: Number,
//   stdDev: Number,
//   gradeCount: Number,
//   sumOfSquares: Number,
//   averageUserGrade: {type: mongoose.Schema.Types.ObjectId, ref: 'GradeScale'}
// }

module.exports = function() {
  var mongoose = require('mongoose');
  var User = require('mongoose').model('User');
  var CoinSeries = require('mongoose').model('CoinSeries');
  var Coins = require('mongoose').model('Coins');
  var CoinGrades = require('mongoose').model('CoinGrades');
  var CoinComments = require('mongoose').model('CoinComments');
  var GradeScale = require('mongoose').model('GradeScale');
  var Q = require('q');

  var db = mongoose.connection;

  //CoinGrades
  var existingGrade1, existingGrade2;

  //CoinComments
  var existingComment1, existingComment2;

  //Coins
  var alreadyGradedCoin, notYetGradedCoin;

  //User
  var testUser1, testUser2;

  //CoinSeries
  var morganSeries, dimeSeries;

  function createTestData(){
    testUser1 = new User({

    });

    testUser2 = new User({

    });

    morganSeries = new CoinSeries({
      series: "Morgan Dollars",
      pcgs_numbers: [1],
      suffixes: []
    });

    dimeSeries = new CoinSeries({
      series: "Mercury Dimes",
      pcgs_numbers: [2],
      suffixes: []
    });

    alreadyGradedCoin  = new Coins({
      pcgs_number: 1,
      country: "USA",
      uploadedBy: testUser1,
      gradedBy: [testUser1, testUser2]
    });

    notYetGradedCoin = new Coins({
      pcgs_number: 1,
      gradedBy: [],
      uploadedBy: testUser1
    });

    notYetGradedCoin2 = new Coins({
      pcgs_number: 1,
      gradedBy: [],
      uploadedBy: testUser1
    });

    existingGrade1  = new CoinGrades({
      coin: alreadyGradedCoin,
      gradedBy: testUser2
    });

    existingGrade2 = new CoinGrades({
      coin: alreadyGradedCoin,
      gradedBy: testUser1
    });

    existingComment1 = new CoinComments({
      body: "Crap coin",
      name: "derp",
      coin: alreadyGradedCoin,
      postedBy: testUser1
    });

    existingComment2 = new CoinComments({
      name: "another name",
      body: "Brilliant Uncirculated",
      coin: alreadyGradedCoin,
      postedBy: testUser2
    });


    //commit to DBQ.all(tasks)
    var saveTasks = [
      testUser1.save(),
      testUser2.save(),
      morganSeries.save(),
      dimeSeries.save(),
      existingComment1.save(),
      existingComment2.save(),
      existingGrade1.save(),
      existingGrade2.save(),
      alreadyGradedCoin.save(),
      notYetGradedCoin.save(),
      notYetGradedCoin2.save()

    ];

    return Q.all(saveTasks);

  };


  function setup(done){
  //  db.db.dropDatabase(function(err, result){
      createTestData().then(function(results) {
        done();
        //console.log(results);
      }, function (err) {
        console.log(err);
      });

  //  });


    return {
      alreadyGradedCoin: alreadyGradedCoin,
      notYetGradedCoin: notYetGradedCoin,
      morganSeries: morganSeries,
      dimeSeries: dimeSeries,
      testUser1: testUser1,
      testUser2: testUser2,
      notYetGradedCoin2: notYetGradedCoin2
    }

  };
//
  function tearDown(done){
    //TODO: combine these promises and finish with done
    done();
      // CoinGrades.remove(existingGrade1, function(err, removed){
      //   CoinGrades.remove(existingGrade2, function(err, removed){
      //     CoinComments.remove(existingComment1, function(err, removed){
      //       CoinComments.remove(existingComment2, function(err, removed){
      //         Coins.remove(notYetGradedCoin, function(err, removed){
      //           Coins.remove(alreadyGradedCoin, function(err, removed){
      //             CoinSeries.remove(morganSeries, function(err, removed){
      //               CoinSeries.remove(dimeSeries, function(err, removed){
      //                 User.remove(testUser1, function(err, removed){
      //                   User.remove(testUser2, function(err, removed){
      //                     done();
      //                   })
      //                 })
      //               })
      //             })
      //           })
      //         })
      //       })
      //     })
      //   })
      // });

    return {
      alreadyGradedCoin: alreadyGradedCoin,
      notYetGradedCoin: notYetGradedCoin,
      morganSeries: morganSeries,
      dimeSeries: dimeSeries,
      testUser1: testUser1,
      testUser2: testUser2
    }

  };
  return {
    setup: setup,
    tearDown: tearDown
  }
}
