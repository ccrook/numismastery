process.env.NODE_ENV = process.env.NODE_ENV || "development";
var port = process.env.PORT || 3030;

var mongoose = require('./config/mongoose');
var express = require('./config/express');
var passport = require('./config/passport');

    // MongoStore = require('connect-mongo')(session)
var errorHandler = require('error-handler');
// var request = require("request");
// var AWS = require('aws-sdk');
//var bucket = config.get(env).aws.bucket;

var db = mongoose();
var app = express();
var passport = passport(app);

app.listen(port);
console.log("Listening on port " + port);



exports.app = app;
