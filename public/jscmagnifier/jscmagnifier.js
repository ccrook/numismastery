/*
Javascript jscMagnifier control
version 1.3
AdvanceByDesign.com

Copyright (C) 2010-2011 Robert Rook
Released under the terms of the
ABD Free Source Code Licence
*/
JSCMAGNIFY_W = 500;
JSCMAGNIFY_H = 500;
JSCMAGNIFY_POS = 3;
JSCMAGNIFY_CLR = "#D00";
JSCMAGNIFY_RATIO = 100;
JSCMAGNIFY_ISDRAWN = false;

function jscMagnify(obj,event) {
	event_x = (event.x?event.x:event.clientX);
	event_y = (event.y?event.y:event.clientY);

	scrpos = GetViewArea();
	event_x+= scrpos['x'];
	event_y+= scrpos['y'];

	tmp = new Image();
	tmp.src = obj.src;
	tmp.width = ((tmp.width/100)*JSCMAGNIFY_RATIO);
	tmp.height = ((tmp.height/100)*JSCMAGNIFY_RATIO);

	box_w = 0;
	box_h = 0;

	box_w = Math.floor(obj.width/(tmp.width/JSCMAGNIFY_W));
	box_h = Math.floor(obj.height/(tmp.height/JSCMAGNIFY_H));

	offsets = OBJ_GetOffsets(obj);

	if(!JSCMAGNIFY_ISDRAWN) {
		var csstxt = 'background:'+JSCMAGNIFY_CLR+';width:1px;height:1px;';
		csstxt+= 'left:-10px;top:-10px;display:block;position:absolute;';
		csstxt+= 'z-index:100;overflow:hidden;border:none;';
		
		var d_t = document.createElement('div');
		d_t.id = 'id_jscmagnify_box_t';
		d_t.style.cssText = csstxt;
		
		var d_r = document.createElement('div');
		d_r.id = 'id_jscmagnify_box_r';
		d_r.style.cssText = csstxt;
		
		var d_b = document.createElement('div');
		d_b.id = 'id_jscmagnify_box_b';
		d_b.style.cssText = csstxt;
		
		var d_l = document.createElement('div');
		d_l.id = 'id_jscmagnify_box_l';
		d_l.style.cssText = csstxt;
		
		var d_i = document.createElement('div');
		d_i.id = 'id_jscmagnify_zoom';
		d_i.style.cssText = 'border:1px solid '+JSCMAGNIFY_CLR+';background-color:#FFF;width:'+JSCMAGNIFY_W+'px;height:'+JSCMAGNIFY_H+'px;left:0;top:0;display:none;position:absolute;z-index:100;overflow:hidden;padding:0;margin:0;';
		
		document.body.appendChild(d_t);
		document.body.appendChild(d_r);
		document.body.appendChild(d_b);
		document.body.appendChild(d_l);
		document.body.appendChild(d_i);
		
		JSCMAGNIFY_ISDRAWN = true;
	}
	if(JSCMAGNIFY_POS<0 || JSCMAGNIFY_POS>9) { JSCMAGNIFY_POS = 3; }

	box_x = (event_x-(box_w/2))<offsets['x']?offsets['x']:(event_x-(box_w/2));
	box_y = (event_y-(box_h/2))<offsets['y']?offsets['y']:(event_y-(box_h/2));

	box_x = (box_x+box_w)>(offsets['x']+offsets['width'])?(offsets['x']+offsets['width']-box_w):box_x;
	box_y = (box_y+box_h)>(offsets['y']+offsets['height'])?(offsets['y']+offsets['height']-box_h):box_y;

	box_x = Math.round(box_x);
	box_y = Math.round(box_y);

	img_x = Math.floor((box_x-offsets['x'])*(tmp.width/obj.width));
	img_y = Math.floor((box_y-offsets['y'])*(tmp.height/obj.height));

	jscBox = GetOBJ("id_jscmagnify_box_t");
	jscBox.style.width = box_w+"px";
	jscBox.style.left = box_x+"px";
	jscBox.style.top = box_y+"px";
	jscBox = GetOBJ("id_jscmagnify_box_r");
	jscBox.style.height = box_h+"px";
	jscBox.style.left = (box_x+box_w)+"px";
	jscBox.style.top = box_y+"px";
	jscBox = GetOBJ("id_jscmagnify_box_b");
	jscBox.style.width = box_w+"px";
	jscBox.style.left = box_x+"px";
	jscBox.style.top = (box_y+box_h)+"px";
	jscBox = GetOBJ("id_jscmagnify_box_l");
	jscBox.style.height = box_h+"px";
	jscBox.style.left = box_x+"px";
	jscBox.style.top = box_y+"px";

	pos_x = (JSCMAGNIFY_POS>4?(box_x-(JSCMAGNIFY_W+12)):(JSCMAGNIFY_POS==0||JSCMAGNIFY_POS==4?(box_x-Math.round((JSCMAGNIFY_W-box_w+2)/2)):(box_x+box_w+10)));
	pos_y = (JSCMAGNIFY_POS==7||JSCMAGNIFY_POS<2?(box_y-(JSCMAGNIFY_H+12)):(JSCMAGNIFY_POS==2||JSCMAGNIFY_POS==6?(box_y-Math.round((JSCMAGNIFY_H-box_h+2)/2)):(box_y+box_h+10)));

	jscBox = GetOBJ("id_jscmagnify_zoom");
	jscBox.innerHTML = '<img src="'+tmp.src+'" width="'+tmp.width+'px" height="'+tmp.height+'px" style="position:relative;left:-'+img_x+'px;top:-'+img_y+'px;">\n';
	jscBox.style.left = pos_x+"px";
	jscBox.style.top = pos_y+"px";
	jscBox.style.display = "block";
}
function jscMagnifyHide() {
	if(!JSCMAGNIFY_ISDRAWN) { return; }
	GetOBJ("id_jscmagnify_box_l").style.left = "-10px";
	GetOBJ("id_jscmagnify_box_r").style.left = "-10px";
	GetOBJ("id_jscmagnify_box_t").style.top = "-10px";
	GetOBJ("id_jscmagnify_box_b").style.top = "-10px";
	GetOBJ("id_jscmagnify_zoom").style.display = "none";
	return;
}

if(typeof GetOBJ!='function') {
	function GetOBJ(objID) {
		if(document.getElementById) {
			return document.getElementById(objID);
		} else if(document.all) {
			return document.all[objID];
		}
		return null;
	}
}
if(typeof OBJ_GetOffsets!='function') {
	function OBJ_GetOffsets(obj) {
		offsets = new Array();
		offsets['width'] = obj.offsetWidth;
		offsets['height'] = obj.offsetHeight;

		offsets['x'] = 0;
		offsets['y'] = 0;

		tmpOBJ = obj;

		do {
			offsets['x']+= tmpOBJ.offsetLeft;
			offsets['y']+= tmpOBJ.offsetTop;
		} while (tmpOBJ = tmpOBJ.offsetParent);

		return offsets;
	}
}
if(typeof GetViewArea!='function') {
	function GetViewArea() {
		scr = new Array();
		scr['x'] = document.body.scrollLeft; scr['y'] = document.body.scrollTop;
		if(window.innerWidth) {
			scr['w'] = window.innerWidth; scr['h'] = window.innerHeight;
		} else {
			scr['w'] = document.body.offsetWidth; scr['h'] = document.body.offsetHeight;
		}
		return scr;
	}
}
