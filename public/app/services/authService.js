angular.module('numismastery').factory('AuthService',
  ['$q', '$timeout', '$http', '$rootScope',
  function ($q, $timeout, $http, $rootScope) {

    // create user variable
    var user = null;
    var userInfo;
    getUser();

    function isLoggedIn() {

      if(user !== null) {
        console.log("USER IS NOT NULL");
        console.log(user != undefined && userInfo != undefined);
        return user && userInfo;
      }else{
        console.log("ELSE");
        //return user;
        return getUser().then(function(latest){
          //console.log("LATEST");
          //console.log(latest);
          user = latest;
          return user;
        });
      }
      return false;
    };

    function getUserStatus() {
      $http.get('/api/user/status')
      // handle success
      .success(function (data) {
        if(data.status){
          user = true;
        } else {
          user = false;
        }
      })
      // handle error
      .error(function (data) {
        user = false;
      });
    }

    // function refreshUser(){
    //   user = null;
    //   return getUser();
    // }

    function getUser() {
      var deferred = $q.defer();
      console.log(user);
      //if(!userInfo || userInfo == null){
        $http.get('/api/user').then(function(response){
          console.log(response);
          if(response.status === 200 && response.data && response.data != ""){
            console.log("DATA");
            console.log(response.data);
            userInfo = response.data;
            user = true;
          }else{
            userInfo = null;
            user = false;

          }
          $rootScope.user = userInfo;
          deferred.resolve(userInfo);
        }, function(errorResponse){
           userInfo = null;
           user = false;
           deferred.reject();
        })
      // }else{
      //   console.log("USER INFO");
      //   console.log(userInfo);
      //   deferred.resolve(userInfo);
      // }
      return deferred.promise;
    };

    function login(username, password) {

      // create a new instance of deferred
      var deferred = $q.defer();

      function loginSuccess(response) {
        console.log("SUCCESS");
        console.log(response.data);
        if(response.status === 200 && response.data.status){
          user = true;
          deferred.resolve();
        } else {
          user = false;
          deferred.reject();
        }
      };

      function loginError(err){
        console.log("ERROR", err);
        user = false;
        deferred.reject(err);
      }
      // send a post request to the server
      var data = {username: username, password: password};
      $http.post('/api/auth/login', data, data).then(loginSuccess, loginError);

        // return promise object
        return deferred.promise;

    };

    function logout() {
      // create a new instance of deferred
      var deferred = $q.defer();

      // send a get request to the server
      $http.get('/logout')
        // handle success
        .success(function (data) {
          user = false;
          userInfo = null;
          deferred.resolve();
        })
        // handle error
        .error(function (data) {
          userInfo = null;
          user = false;
          deferred.reject();
        });

      // return promise object
      return deferred.promise;

    };

    function register(form) {

      // create a new instance of deferred
      var deferred = $q.defer();

      // send a post request to the server
      $http.post('/api/auth/signup', form)
        // handle success
        .success(function (data, status) {
          if(status === 200 && data.status){
            deferred.resolve(data);
          } else {
            deferred.reject(data);
          }
        })
        // handle error
        .error(function (data) {
          deferred.reject(data);
        });

      // return promise object
      return deferred.promise;

    }

    function getEmailFromToken(token){
      console.log("AUTH SERVICE");
      return $http.get("/api/user/token/" + token);
    }

    function forgotPassword(email){
      return $http.post("/api/auth/forgotpassword", {email: email});
    }

    function resetPassword(token, newPassword, confirmPassword){
      return $http.post("/api/auth/resetpassword", {token: token, password: newPassword, confirm: confirmPassword});
    }

    function enableUser(user, callback){
      if(!user)
        user = userInfo;
      return $http.post("/api/user/" + user._id + "/enable");
    }

    function disableUser(user, callback){
      if(!user)
        user = userInfo;
      return $http.post("/api/user/" + user._id + "/disable");
    }

    function acceptTerms(successCallback, failureCallback){
      //Send current userid
      return $http.post("/api/user/" + userInfo._id + "/acceptterms").then(successCallback, failureCallback);
    }

    function displayNameTaken(displayName, callback){
      return $http.get("/api/user/displayname/" + displayName).then(function(response){
        callback(null, response.data);
      }, function(error){
        callback(error, null);
      });
    }

    function verifyRecaptcha(form, callback){
      return $http.post("/api/recaptcha/verify", form, form).then(function(successResponse){
        callback(null, successResponse.data);
      }, function(failureResponse){
        callback(failureResponse.data);
      });
    }


    // return available functions for use in controllers
    return ({
      isLoggedIn: isLoggedIn,
      getUserStatus: getUserStatus,
      getUser: getUser,
      login: login,
      logout: logout,
      register: register,
      forgotPassword: forgotPassword,
      resetPassword: resetPassword,
      getEmailFromToken: getEmailFromToken,
      enableUser: enableUser,
      disableUser: disableUser,
      acceptTerms: acceptTerms,
      displayNameTaken: displayNameTaken,
      verifyRecaptcha: verifyRecaptcha
    });
}]);
