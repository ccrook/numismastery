angular.module('numismastery').factory('CoinService',
  ['$q', '$timeout', '$http',
  function ($q, $timeout, $http) {

    var get = function (id){

    };

    var insert = function (id){

    };

    var update = function (id){

    };

    var del = function (id){

    };

    function comments(coinId, page, pageSize){
        // create a new instance of deferred
        var deferred = $q.defer();

        // send a post request to the server
        $http.get('/api/coin/' + coinId + '/comments?page=' + page + "&perPage=" + pageSize)
            // handle success
            .success(function (response) {
                deferred.resolve(response);
            })
            // handle error
            .error(function (errorResponse) {
              user = false;
              deferred.reject();
            });

          // return promise object
          return deferred.promise;
    }


    // return available functions for use in controllers
    return ({
      get: get,
      insert: insert,
      update: update,
      del: del,
      comments: comments
    });
}]);
