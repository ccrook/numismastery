angular.module('numismastery').factory('UserProfileService',
  ['$q', '$timeout', '$http',
  function ($q, $timeout, $http) {

    //TODO: default/limit these?
    function comments(page, pageSize) {

      // create a new instance of deferred
      var deferred = $q.defer();

      // send a post request to the server
      $http.get('/api/user/comments?page=' + page + "&perPage=" + pageSize)
          // handle success
          .success(function (response) {
              deferred.resolve(response);
          })
          // handle error
          .error(function (errorResponse) {
            user = false;
            deferred.reject();
          });

        // return promise object
        return deferred.promise;

    };

    function grades(page, pageSize) {
      // create a new instance of deferred
      var deferred = $q.defer();

      // send a post request to the server
      $http.get('/api/user/coins/graded?page=' + page + "&perPage=" + pageSize)
          // handle success
          .success(function (response) {
              deferred.resolve(response);
          })
          // handle error
          .error(function (errorResponse) {
            user = false;
            deferred.reject();
          });

        // return promise object
        return deferred.promise;
    };

    function uploadedCoins(page, pageSize) {
      // create a new instance of deferred
      var deferred = $q.defer();

      // send a post request to the server
      $http.get('/api/user/coins/uploaded?page=' + page + "&perPage=" + pageSize)
          // handle success
          .success(function (response) {
              deferred.resolve(response);
          })
          // handle error
          .error(function (errorResponse) {
            user = false;
            deferred.reject();
          });

        // return promise object
        return deferred.promise;
    };

    // return available functions for use in controllers
    return ({
      comments: comments,
      grades: grades,
      uploadedCoins: uploadedCoins
    });
}]);
