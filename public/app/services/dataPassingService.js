angular.module("numismastery").factory('dataPassingService', function() {
 var savedData = {}
 function set(key, value) {
   savedData[key] = value;
 }
 function get(key) {
   if(!key)
    return savedData;
  return savedData[key];
 }


 return {
  set: set,
  get: get
 }

});
