angular.module("numismastery").controller("coinUploadCtrl", ['$scope', '$location', "$http", '$filter', '$timeout', "Upload", 'coinSeries', 'AuthService',
 function($scope, $location, $http, $filter, $timeout, Upload, coinSeries, AuthService){

  //TODO: totally re-do this

    $scope.coinToUpload = {
      pcgs_number: null,
      denomination: null,
      year: null,
      mintmark: null,
      images: {
        obverse: [],
        reverse: [],
        edge: []
      },
      proof: 0
    };

    $scope.lookups = {};

    $scope.lookups.mintmarks = [
      { value: "P", text: "Philadelphia"},
      { value: "D", text: "Denver"},
      { value: "D", text: "Dahlonega"},
      { value: "S", text: "San Francisco"},
      { value: "C", text: "Charlotte"},
      { value: "CC", text: "Carson City"},
      { value: "O", text: "New Orleans"},
      { value: "W", text: "West Point"},
    ];


    if(coinSeries){
      $scope.lookups.coinSeries = coinSeries.data;
    }
    var orderBy = $filter('orderBy');
    $scope.predicate = '_id';
    $scope.reverse = false;

    $scope.mintmarkpredicate = "text";
    $scope.mintmarkrevese = false;

    $scope.proof = function(proof) {
      return function(series){
        return ( series.proof === proof );
      }
    }

    $scope.order = function(predicate) {
      $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
      $scope.predicate = predicate;
    };


  //TODO: user to upload coin
  //TODO: select denomination
  //TODO: select country
  //TODO: select year & mint mark
  //TODO: select variety?
  //TODO: select multiple images, tag  one each as obverse and reverse

  function uploadSuccess(resp){
    toastr.success("Coin uploaded!  You will be able to view this from your user profile page to monitor crowd grading.  Redirecting you to the coin profile page.");
    $timeout(function(){

      $location.path("/grading-results/" + resp.data.coinId);
    }, 3000);

      //console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
  }

  function uploadFailure(resp){
    console.log('Error status: ' + resp.status);
    toastr.error("Error: " + resp.status);
  }

  $scope.upload = function (file) {

    //TODO: verify form is filled out properly

    AuthService.verifyRecaptcha({ 'g-recaptcha-response' : $scope.coinToUpload['g-recaptcha-response']}, function(err, recaptchaResponse){
      //$scope.coinToUpload.proof = $scope.proofs;
      if(err) return uploadFailure(err);
      //if(err) return res.status(400).json({status: "error", err });
      //CPC: currently buffering through our server before going to S3 (not enough time to implement direct posts)
        Upload.upload({
            url: '/upload',
            data: $scope.coinToUpload, //TODO: pass selected data
            transformRequest: angular.identity
        }).then(uploadSuccess, uploadFailure);
    });
  };

    // // for multiple files:
    // $scope.uploadFiles = function (files) {
    //   if (files && files.length) {
    //     // for (var i = 0; i < files.length; i++) {
    //     //   Upload.upload({..., data: {file: files[i]}, ...})...;
    //     // }
    //     // or send them all together for HTML5 browsers:
    //     Upload.upload({data: {file: files}});
    //   }
    // };
}]);


  // $scope.login = function(){
  //   console.log("grading!");
  //     navigator.id.get(function(assertion) {
  //       if (assertion) {
  //         $("input").val(assertion);
  //         $("form").submit();
  //       } else {
  //         location.reload();
  //       }
  //     });
  //
  // };
