angular.module("numismastery").controller("mainCtrl", ["$scope", '$rootScope', '$timeout', "AuthService", "$state", function( $scope, $rootScope, $timeout, AuthService, $state){
  //CPC: pretty sure we dont need this anymore
  //
  // $scope.authService = AuthService;
  //
  // $rootScope.toggleHeader = function(){
  //   $(".wide").slideUp({
  //     complete: function(){
  //       $rootScope.headerVisible = !$rootScope.headerVisible;
  //       //TODO: write this to a cookie / local storage so we don't keep showing it
  //     }
  //   });
  // };
  //
  //
  // if(!$rootScope.user){
  //   AuthService.getUser().then(function(response){
  //     $rootScope.user = response;
  //   });
  // }
  // $rootScope.headerVisible = $rootScope.headerVisible || true;
  $scope.getStarted = function(){
    if($rootScope.user){
      $state.go("start-grading");
    }else{
      $state.go("login");
    }
  };
}]);
