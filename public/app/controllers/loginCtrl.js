angular.module("numismastery").controller("loginCtrl",
['$rootScope', '$scope', '$location', '$state', '$stateParams', '$http', '$filter', '$timeout', 'AuthService',
function($rootScope, $scope, $location,$state, $stateParams, $http, $filter, $timeout, AuthService){
  //TODO: check if signed in, if so, redirect
  $scope.getAuthUrl = function(baseUrl){
    var redirectUrl = $rootScope.toUrl ? $rootScope.toUrl : $stateParams.toState.url;
    console.log("using" + redirectUrl);
    redirectUrl = encodeURIComponent(redirectUrl);
    return $stateParams.toState && $stateParams.toState.url && $stateParams.toState.url != "/login" ? baseUrl + "?redirectUrl=" + redirectUrl : baseUrl;
  }
  $scope.redirectTo = "";

      // if (AuthService.isLoggedIn()) {
      //   AuthService.getUser().then(function(user){
      //     $scope.$emit("userUpdated", user);
      //     $location.path("/");
      //   })
      //
      //
      // }

      $scope.signUp = function(){
        $location.path("/signup");
      }

      $scope.login = function () {

        // initial values
        $scope.error = false;
        $scope.disabled = true;

        // call login from service
        AuthService.login($scope.username, $scope.password)
          // handle success
          .then(function (response) {
            $rootScope.$broadcast("userUpdated");
            console.log($stateParams.toState);
            if($stateParams.toState){
              $location.path($stateParams.toState);
            }else{
              $state.go('home');
            }

            $scope.disabled = false;
            $scope.loginForm = {};
          }).fail(function(error){
            $scope.error = true;
            $scope.errorMessage = error.err;
            $scope.disabled = false;
          })
          // handle error
          .catch(function (ex) {
            $scope.error = true;
            $scope.errorMessage = "Invalid username and/or password";
            $scope.disabled = false;
            $scope.loginForm = {};
          });

      };


}]);
