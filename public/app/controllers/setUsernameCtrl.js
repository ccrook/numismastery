angular.module("numismastery").controller("setUsernameCtrl", ['$scope', '$rootScope', '$state', '$timeout', '$http', 'AuthService', function($scope, $rootScope, $state, $timeout, $http, AuthService){

    $scope.errors = [];
    $scope.submitting = false;

    $scope.submitUsername = function(){
      $scope.submitting = true;
      $scope.errors = [];
      $http.post("/auth/setusername", { username: $scope.username }).then(function(response){
        toastr.success("Username set to " + $scope.username + "! You will be redirected to the home page in 3 seconds.");
        //$rootScope.user = response.data;
        AuthService.getUser().then(function(userInfo){
          //$scope.$emit("userUpdated", response.data);
          //redirect after timeout
          $timeout(function(){
            $state.go("home");
            //$scope.$apply();
          }, 3000);
        })

        //  $location.replace();
      //  }, 3000);

      }, function(err){
        $scope.submitting = false;
        $scope.errors = err.data;

      })

    };


}]);
