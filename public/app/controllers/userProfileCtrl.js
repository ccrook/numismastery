angular.module("numismastery").controller("userProfileCtrl", ['$scope', '$location', "$http", 'NgTableParams', 'comments', 'UserProfileService', 'grades',
function($scope, $location, $http, NgTableParams, comments, UserProfileService, grades){

    //TODO: allow user to set UserName
    //TODO: allow user to see a list of coins they've uploaded
    //TODO: show summary stats for user (i.e. overall accuracy, std dev by true grade level)
    //TODO: see total grade count, broken down by denomination & series
    $scope.uploadedCoins = [];

    //TODO: get uploaded coins

    $scope.userStats = {};
    $scope.comments = comments;
    $scope.accountInfo = {};
    $scope.editing = false;

    $scope.grades = grades;

    $scope.submit = function(){
      $scope.editing = false;
      UserProfileService.updateUser($scope.accountInfo).then(function(response){
        $scope.editing = false;
      }, function(errorResponse){

      });
    };

    function deleteSuccess(response){
      toastr.success(response.data);
      //Reload the uploaded coins params
      $scope.uploadedCoinsParams.reload();
    }

    function deleteFailure(err){
      toastr.error("Error deleting coin: " + err.data);

    }

    $scope.deleteCoin = function(coinId){
      console.log("CLICKED");
      var deleteConfirmed = confirm("Are you sure you want to delete this coin?  There is no way to undo this operation.  Note: If this coin hasn't yet been graded, you will be refunded an upload credit.")
      if(deleteConfirmed){
        $http.delete("/coin/" + coinId).then(deleteSuccess, deleteFailure);
      }

    };

    $scope.toggleEditing = function(){
      $scope.editing = !$scope.editing;
    };

    $scope.commentsParams = new NgTableParams({
      page: 1, // show first page
      count: 10 // count per page
    }, {
      filterDelay: 300,
      getData: function(params) {
        // ajax request to api
        console.log(params);

        return UserProfileService.comments(params.page(), params.count()).then(function(response){
          params.total(response.total);
          return response.results;
        });

        // return Api.get(params.url()).$promise.then(function(data) {

        //   return data.results;
        // });
      }
    });

    $scope.gradesParams = new NgTableParams({
      page: 1, // show first page
      count: 10 // count per page
    }, {
      filterDelay: 300,
      getData: function(params) {
        // ajax request to api
        console.log(params);

        return UserProfileService.grades(params.page(), params.count()).then(function(response){
          params.total(response.total);
          return response.results;
        });


        // return Api.get(params.url()).$promise.then(function(data) {

        //   return data.results;
        // });
      }
    });

    $scope.uploadedCoinsParams = new NgTableParams({
      page: 1, // show first page
      count: 10 // count per page
    }, {
      filterDelay: 300,
      getData: function(params) {
        // ajax request to api
        console.log(params);

        return UserProfileService.uploadedCoins(params.page(), params.count()).then(function(response){
          params.total(response.total);
          return response.results;
        });

        // return Api.get(params.url()).$promise.then(function(data) {

        //   return data.results;
        // });
      }
    });

}]);

  // $scope.login = function(){
  //   console.log("grading!");
  //     navigator.id.get(function(assertion) {
  //       if (assertion) {
  //         $("input").val(assertion);
  //         $("form").submit();
  //       } else {
  //         location.reload();
  //       }
  //     });
  //
  // };
