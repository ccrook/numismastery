angular.module("numismastery").controller("resetPasswordCtrl",
  ['$scope', '$location', "$http", '$filter', '$timeout', 'AuthService', '$stateParams', 'email', '$state',
  function($scope, $location, $http, $filter, $timeout, AuthService, $stateParams, email, $state){

    function resetSuccess(){
      toastr.success("Successfully reset password.");
      $timeout(function(){
        $state.go("login");
      }, 3000);

    }

    function resetFailure(err){
      toastr.error("Failed to reset password.");
      $scope.error = err;
    }

    $scope.email = email.data;
    $scope.password;
    $scope.confirm;

    //extract token from state params?
    $scope.resetPassword = function(){
      //TODO: error checking on password and confirm before allowig this
      AuthService.resetPassword($stateParams.token, $scope.password, $scope.confirm).then(resetSuccess, resetFailure);
    };
}]);
