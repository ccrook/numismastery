angular.module("numismastery").controller("signUpCtrl", ['$scope', '$state', '$http', 'AuthService', function($scope, $state, $http, AuthService){

    $scope.errors = [];
    $scope.submitting = false;
    $scope.signUpForm = {};

    $scope.displayNameTaken = false;
    $scope.emailsMatch = true;
    $scope.passwordsMatch = true;


    $scope.$watch(function(scope){
      return scope.signUpForm.displayName;
    }, function(newValue,oldValue){

      AuthService.displayNameTaken(newValue, function(err, response){
        $scope.displayNameTaken = response.taken;
      }, function(error){
        toastr.error(error);
      });
    });

    function emailsMatch(){
      return $scope.signUpForm.email === $scope.signUpForm.confirm_email;
    }

    function passwordsMatch(){
      return $scope.signUpForm.password === $scope.signUpForm.confirm_password;
    }

    $scope.submit = function(){

      $scope.submitting = true;
      $scope.errors = [];
      if(!emailsMatch()){
        $scope.errors.push("Email addresses must match.");
      }
      if(!passwordsMatch()){
        $scope.errors.push("Passwords must match.");
      }
      if($scope.displayNameTaken == true){
        $scope.errors.push("Display name already taken.  Please choose another.");
      }
      if($scope.errors.length > 0){
        $scope.submitting = false;
        grecaptcha.reset();
        return false;
      }
      else if($scope.displayNameTaken == false){
        $scope.errors = [];
        AuthService.register($scope.signUpForm).then(function(response){
          console.log(response);
          toastr.success("Congratulations!  Account successfully created.  Redirecting to login page...", "", {timeOut: 2000, onHidden: function(){ $state.go("login");} });

        }, function(err){
          grecaptcha.reset();
          $scope.submitting = false;
          if(err.status == "error"){
            if(typeof(err.errors) == "object" && err.errors.length){
              $scope.errors = err.errors;
            }
            else{
              $scope.errors.push(err.errors);
            }
          }

        });
      }else{
        $scope.submitting = false;
      }

    };


}]);
