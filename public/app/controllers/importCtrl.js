angular.module("numismastery").controller("importCtrl", ["$scope", '$rootScope', '$timeout', "AuthService", "$state", '$http', function( $scope, $rootScope, $timeout, AuthService, $state, $http){

  var MESSAGES = {
    success: "Successfully imported!",
    error: "Error importing coins."
  };

  $scope.importInProgress = false;

  $scope.progressText = "Derp 123";
  $scope.progressValue = 50;
  $scope.progressStyle = function(){
    return {
      'width': $scope.progressValue.toString() + '%'
    };
  };


  function importSuccessHandler(successResponse){
    $scope.importInProgress = false;
    //TODO: show success text
    console.log(successResponse);
    //TODO: return status by cert number (i.e. not found, already uploaded, no pictures, etc)


  };

  function importErrorHandler(errorResponse){
    console.log(errorResponse);
    $scope.importInProgress = false;
  };

  $scope.importCoins = function(){
    if(!$scope.importInProgress){
      //TODO: split cert numbers by line breaks, send to server
      var certNumberArray = $scope.certNumbers.split('\n');
      if(certNumberArray.length > 10){
        return toastr.error("There is a 10 coin maximum.");
      }

      $http.post("/api/import/" + $scope.thirdPartyGrader, {certNumbers: certNumberArray}).then(importSuccessHandler, importErrorHandler);

      $scope.importInProgress = true;

      // setInterval(function () {
      //   //TODO: poll server for progress?  or use sockets
      // }, 100);
    }

  };
}]);
