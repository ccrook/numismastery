angular.module("numismastery").controller("startGradingCtrl", function($scope, $rootScope, $routeParams, $location, $filter, $state, coinSeries){

  $scope.lookups = {};
  $scope.proofs = 0;

  if(coinSeries){
    $scope.lookups.coinSeries = coinSeries.data;
  }

  var orderBy = $filter('orderBy');
  $scope.predicate = '_id';
  $scope.reverse = false;

  $scope.proof = function(proof) {
    return function(series){
      return ( series.proof === proof );
    }
  }

  $scope.order = function(predicate) {
    $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
    $scope.predicate = predicate;
  };

  $rootScope.fields = {
    selectedSeries: $routeParams.coinSeries || ""
  };

  $rootScope.$watch(function(rootScope){
    return $rootScope.fields.selectedSeries;
  }, function(newValue, oldValue){
    console.log("old", oldValue, "new", newValue);
    if(newValue && newValue != oldValue){
      $state.go("grade", { coinSeries: newValue._id });
    }
  });

});
