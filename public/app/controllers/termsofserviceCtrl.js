angular.module("numismastery").controller("termsOfServiceCtrl", function($scope, $rootScope, $routeParams, $location, $filter, $state, AuthService){

  $scope.accept = function(){
    //TODO:
    AuthService.acceptTerms(function(response){
      //$state.go("home", {}, {reload: true});
      //CPC: this isn't redirecting properly because the user isn't refreshed yet
      //Try forcing via location
      console.log(response);
      //$rootScope.user = response.data;
      AuthService.getUser().then(function(userInfo){
        if(!$rootScope.user.displayName){
          return $state.go("setusername");
        }
        return $state.go("home");
      });

    }, function(failureResponse){
      toastr.error(failureResponse);
    });
  };

  $scope.decline = function(){
    //TODO: disable user and redirect to sign out
    AuthService.disableUser(function(err, response){
      $location.path("/logout");
    });
  };

});
