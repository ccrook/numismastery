angular.module("numismastery").controller("forgotPasswordCtrl",
  ['$scope', '$location', "$http", '$filter', '$timeout',  'AuthService', '$state',
  function($scope, $location, $http, $filter, $timeout,  AuthService, $state){

    function forgotPassHandler(err){
      //Do the same thing so we don't reveal which accounts are actually real
      toastr.success("Password reset email sent to " + $scope.email);
      $timeout(function(){
        $state.go("login");
      }, 3000);
    }

    $scope.email;
    $scope.sendPasswordReset = function(){
      $scope.error = undefined;
      if($scope.email && $scope.email != ""){
        AuthService.forgotPassword($scope.email).then(forgotPassHandler, forgotPassHandler);
      }else{
        $scope.error = "Email is required.";
      }

    };
}]);
