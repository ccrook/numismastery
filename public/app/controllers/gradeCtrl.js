angular.module("numismastery").controller("gradeCtrl", ['$rootScope', '$scope', '$location', '$stateParams', '$state',
'$http', '$filter', '$timeout', 'coinToGrade', 'grades', 'dataPassingService', 'AuthService', function($rootScope, $scope, $location, $stateParams, $state,
   $http, $filter, $timeout, coinToGrade, grades, dataPassingService, AuthService){

  //In case we're requiring a recaptcha after X grades
  $scope.recaptchaRequired = coinToGrade.data === true;
  $scope.recaptchaForm = {};


  $scope.coinReportingReasons = [
    "Not a Coin",
    "Offensive Content",
    "Copyrighted Image",
    "Incorrect Year",
    "Incorrect Series",
    "Incorrect Strike Type",
    "Non-U.S. Coin",
    "Miscellaneous"
  ];

$scope.reportSubmitting = false;
$scope.reportCoinData = {

};


  $scope.verifyRecaptcha = function(){
    AuthService.verifyRecaptcha($scope.recaptchaForm, function(err, result){
      if(err) $state.reload();
      $scope.recaptchaRequired = false;
    });
  };

  // if(coinToGrade && (!coinToGrade.data || coinToGrade.data === null)){
  //   console.log("************REROUTE TO SELECT SERIES****");
  //   toastr.error("Whoops!  We ran out coins of series \"" + $rootScope.fields.selectedSeries.series + "\" for you to grade!  Please choose another series.");
  //   $state.go("start-grading");
  // }

  $scope.coinToGrade = coinToGrade ? coinToGrade.data : undefined;
  $rootScope.grades = grades.data;
  $scope.lookups = {};
  $scope.imageTags = [];
  $scope.lookups.seriesDesignation = [];
  $scope.lookups.problemCoinDesignation = [];
  $scope.lookups.seriesVarieties = [];
  $scope.lookups.suffix = [];

  if(!$scope.recaptchaRequired && $scope.coinToGrade && $scope.coinToGrade.image_url){
    $scope.coinToGrade.image_url = $scope.coinToGrade.image_url.replace("CertVerificationThumb2", "extralarge");
  }
  else if(!$scope.recaptchaRequired){
    $rootScope.errorMsg = "We ran out of coins for this series!  You'll be redirected to the series selection page shortly..."
    $rootScope.errorRedirect = {
      url:  "/start-grading",
      delaySeconds: "5"
    }
    $location.path("/error");
  }

  if(typeof(grades) != "undefined"){
    $scope.lookups.grades = [];
    for(var i=0;i<grades.data.length;i++){
      //Re-work grade scale data
      $scope.lookups.grades.push($.extend({}, grades.data[i], { value: parseInt(grades.data[i].value) }));
    }
  }

  console.log("STATE DATA");
  console.log($state.current.data);
  if($rootScope.fields.selectedSeries && $rootScope.fields.selectedSeries.proof){
    $scope.lookups.grades = $scope.lookups.grades.filter(function(value){
      return value.text.indexOf("PR") > -1;
    });
  }else{
    $scope.lookups.grades = $scope.lookups.grades.filter(function(value){
      return value.text.indexOf("PR") < 0;
    });
  }

  $scope.gradeSliderOptions = {
    min: 0,
    max: $scope.lookups.grades.length-1,
    step: 1,
    ticks: [
      'test'
    ]
  };

  var orderBy = $filter('orderBy');
  $scope.predicate = 'Value';
  $scope.reverse = false;
  $scope.order = function(predicate) {
    $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
    $scope.predicate = predicate;
  };

  //TODO: for this series, what sort of grade modifiers are there?
  // $scope.gradeModifiers = ["Cleaned", "Damaged - Environmental", "Damaged - Tooling", "Damaged - PVC"];
  // $scope.seriesModifiers = ["FS (Full Steps)", "FB (Full Bars)"];

  //$scope.selectedSeries = $stateParams.coinSeries || "";

  $scope.gradeData = {
    selectedUserGrade: null,
    suffix: null,
    cert_number: null,
    estimatedValue: null,
    problemCoinDesignation: null,
    seriesDesignation: null,
    varietyDesignation: null

  };

  $scope.selectedGradeModifier = null;
  $scope.selectedSeriesModifier = null;

  $scope.comments = "";

    $scope.submitUserGrade = function(){
      $scope.gradeData.coinId = $scope.coinToGrade._id;

      if($scope.gradeData.selectedUserGrade){
        $http.post("/coin", $scope.gradeData).then(function(response){
          toastr.success("Grade submitted!  Retrieving results...")
          $state.go("grading-result", {coinId: $scope.coinToGrade._id}, {reload: true});

        }, function(err){
          if(err){
            toastr.error(err.data);
          }

        //  alert("Argh, something is wrong when submitting your grade!")
        });
      }else{
        toastr.error("Please select a grade using the slider below.");
      }

    };


    function reportSuccess(){
      toastr.success("Report submitted.  This coin will be removed from the grading pool until it is reviewed.");
      $state.reload();
      //Clear the data
      $scope.reportCoinData = {};
      $("#reportCoinModal").modal("hide");
      //Hack because something is leaving the modal open
      $(".modal-backdrop").remove();
    }

    function reportFailure(err){
      toastr.error("Error submitting report: " + err.data);
      $scope.reportSubmitting = false;
    }

    $scope.showReportCoinDialog = function(){
      $("#reportCoin").dialog("show");
    };

    $scope.reportCoin = function(){

        $scope.reportSubmitting = true;
        //Make sure we have both a reason and a comment
        if(!$scope.reportCoinData.reason || $scope.reportCoinData.reason.length == 0){
          toastr.error("Please select a reason why you are reporting this coin.");
          $scope.reportSubmitting = false;
        }
        if(!$scope.reportCoinData.comments || $scope.reportCoinData.comments.length == 0){
          toastr.error("Please enter comments for your report.");
          $scope.reportSubmitting = false
        }

        var confirmReport = confirm("Are you sure you wish to report this coin?");

        if(confirmReport && $scope.reportSubmitting == true){
            $http.post("/coin/" + $scope.coinToGrade._id + "/report", $scope.reportCoinData).then(reportSuccess, reportFailure);
        }
        return false;

    };

    $scope.changeSeries = function(){
        $state.go("start-grading");
    };

}]);
