angular.module("numismastery").controller("gradingResultsCtrl", ['$scope', '$rootScope', '$location', "$http", '$filter', '$stateParams', '$state', 'gradingResults', 'grades', 'CoinService', 'NgTableParams',
function($scope, $rootScope, $location, $http, $filter, $stateParams, $state, gradingResults, grades, CoinService, NgTableParams){

  $scope.grades = grades;

  //Results include coin
  $scope.gradingResults = gradingResults;
  $scope.awsUrl = "https://s3.amazonaws.com/numismastery/";
  //If they've selected a series (not just viewing coins) allow them to view another, otherwise force series selection
  $scope.grade_another = function(){
    if($rootScope.fields.selectedSeries){
      $state.go("grade", {coinSeries: $rootScope.fields.selectedSeries.series}, {reload: true});
    }
    else{
      $state.go("start-grading");
    }
  }


  $scope.commentsParams = new NgTableParams({
    page: 1, // show first page
    count: 10 // count per page
  }, {
    filterDelay: 300,
    getData: function(params) {
      // ajax request to api
      console.log(params);

      return CoinService.comments(gradingResults._id, params.page(), params.count()).then(function(response){
        params.total(response.total);
        return response.results;
      });

      // return Api.get(params.url()).$promise.then(function(data) {

      //   return data.results;
      // });
    }
  });

  //If multiple images, allow selection
  $scope.select = function(image){
    $scope.selected_image = image;
  }

  $scope.changeSeries = function(){
      $state.go("start-grading");
  };

}]);
