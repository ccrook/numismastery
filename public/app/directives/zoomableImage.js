angular.module('csl.zoomableImage', [])

.directive('zoomableImage', function($compile) {
  return {
    restrict: "A",

    link: function(scope, element, attrs, ngModel){
      //TODO: table "zoomability" based on a scope variable set as an attribute
      $(element).on("mousemove", function(event){
        jscMagnify(this,event);
      });
      $(element).on("mouseout", function(event){
        jscMagnifyHide();
      });
    }

  }
});
