angular.module('csl.resizableDraggable', [])
.directive('resizableDraggable', function () {

    return {
        restrict: 'A',
        scope: {
            resizeCallback: '&onResize',
            dropCallback: '&onDrop'
        },
        link: function postLink(scope, elem, attrs) {
            elem.resizable();
            elem.on('resize', function (evt, ui) {
              scope.$apply(function() {
                if (scope.resizeCallback) {
                  scope.resizeCallback({$evt: evt, $ui: ui });
                }
              })
            });

            elem.draggable();
            elem.on('dragstop', function (evt, ui) {
              scope.$apply(function() {
                if (scope.dropCallback) {
                  scope.dropCallback({$evt: evt, $ui: ui });
                }
              })
            });
        }
    };
  })
