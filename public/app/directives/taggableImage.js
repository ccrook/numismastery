angular.module('csl.taggableImage', [])

.directive('taggableImage', function($compile) {
  return {
    restrict: "A",
    require: "ngModel",

    ///template: 'Name: {{customer.name}} Address: {{customer.address}}',
    link: function(scope, element, attrs, ngModel){
      console.log("LINK FOR TAGGING")
      var defaultConfig = {

      };
      var imageTags = [];

      var directiveOptions = angular.copy(scope.$eval(attrs.taggableImage));
      var options = angular.extend(directiveOptions || {}, defaultConfig);

      ngModel.$setViewValue()


      $(element).click(function(evt){
        //TODO: ability to toggle tagging on/off
        //TODO: check if last tag box was actually submitted, otherwise, remove it and add this one

        //PageY & pageY
        console.log(evt.offsetX);
        console.log(evt.offsetY);
        //In case we want to store the locations based on the native resolution of the image
        console.log(element[0].naturalHeight);
        console.log(element[0].naturalWidth);

//        controller.setParentElement(element.parent());

        //TODO: restrict movement and resizability to image boundary
        //TODO: put CSS in class, add hover option where border becomes thicker
        var resizableBox = $("<div resizable-draggable style='border: 1px solid red; width: 80px; height: 80px; position: absolute; top: " + (evt.offsetY - 40) + "px; left: " + (evt.offsetX - 40) + "px; z-index: 1000; background-color: transparent;'  />")
        //Compile since we're generating an element that uses another directive
        var resizableBox = $compile(resizableBox)(scope);

        element.parent().append(resizableBox);
        imageTags.push(resizableBox);

        $(window).on("keypress", function(evt){
          console.log(evt);
          switch(evt.key){
            case "Escape":
              //TODO: fix this so it only removes the last element, not event element
              var tagToRemove = imageTags.pop();
              tagToRemove.remove();
              break;
            case "Enter":
              alert("this is supposed to pop a dialog into which the user will enter information about the highlighted section of the image")
              break;
          }


        });


        //TODO: create and show a dialog box allowing user to
      })
    }
  };
});
