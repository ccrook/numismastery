angular.module('numismastery.filters', []).filter('letterGrade', function() {
  return function(input, grades) {
    //TODO: map to list of grades
    console.log(input);
    if(input){
      return grades[input].grade;
    }
    return "N/A";
  };
});
