
angular.module("numismastery").service('authInterceptor', ['$rootScope', '$q', "$location", function ($rootScope, $q, $location) {
  var service = this;
  service.responseError = error;

      // function success(response) {
      //     return response;
      // }
      function error(response) {
          var status = response.status;
          if (status == 401) {
              //AuthFactory.clearUser();
              toastr.error(response);
              console.log("INTERCEPTED!");
              console.log($location.path());
              //console.log($state.current);
              //$rootScope.toState = $state.current;
              //$state.go("login", {toState: $state.current});
              $rootScope.toUrl = $location.path();
              //$location.path("/login");
              return;
          }
          // otherwise
          return $q.reject(response);
      }
      // return function (promise) {
      //     return promise.then(success, error);
      // }
  }]);
//     var service = this;
//
//     service.responseError = function(response) {
//         if (response.status == 401){
//             window.location = "/login";
//         }
//         return $q.reject(response);
//     };
// })

angular.module("numismastery").config(function($stateProvider, $urlRouterProvider,$locationProvider, $httpProvider){//}, $routeProvider, $locationProvider){
  $locationProvider.html5Mode({
  enabled: true,
  requireBase: false
  });

  //CPC: Add an interceptor to handle redirection when not logged in

  $httpProvider.interceptors.push('authInterceptor');

   $urlRouterProvider.otherwise('/home');

    $stateProvider
    // .state("main", {
    //   controller: 'mainCtrl',
    //   controllerAs: 'main',
    // })
    .state('home', {

         url: '/home',
         templateUrl: 'partials/main',
         controller: 'mainCtrl',
         controllerAs: 'main',
         data: {
           activeTab: 'home',
           access: {restricted: false}
       }
     }).state("start-grading", {
          url: "/start-grading",
         templateUrl: "/partials/start-grading",
         controller: "startGradingCtrl",
         resolve: {
           coinSeries: function($http){
             return $http.get("/coin-series/");
           }
         },
         data: {
           access: {restricted: true}
         }

       }).state("grade", {
           url: "/grade/:coinSeries",
           templateUrl: "/partials/grade",
           controller: "gradeCtrl",
           resolve: {
             grades: function($http){
               return $http.get("/grades", function(response){
                 return response.data;
               });
             },
             coinToGrade: function($http, $stateParams, $q){
               //TODO: change this to a service...
               var deferred = $q.defer();
               $http.get('/coinFromSeries/' + encodeURIComponent($stateParams.coinSeries)).then(function(success){
                 deferred.resolve(success);
               }, function(err){
                 if(typeof(err) == 'string'){
                   deferred.reject(err);
                 }
                 deferred.reject(err.data.message);
               })
               return deferred.promise;
             }
           },
           data:{
             access: {restricted: true}
           }

         }).state("grading-result", {
           url: "/grading-results/:coinId",
           templateUrl: "/partials/grading-result",
           controller: "gradingResultsCtrl",
           resolve: {
             //TODO: user services for these resolves
               gradingResults: function($http, $stateParams){
                   return $http.get('/coin/' + $stateParams.coinId + "/grading-results").then(function(response){
                     return response.data;
                   });
               },
               grades: function($http){
                 return $http.get("/grades").then(function(response){
                   return response.data;
                 });
               }
           },
           access: {restricted: false}
         }).state("error", {
       url: "/error",
         templateUrl: "/partials/error",
         controller: "mainCtrl",
         data: {
           access: {restricted: false}
         }
       }).state("error-not-found", {
         url: "/error/not-found",
           templateUrl: "/partials/error",
           controller: "mainCtrl",
           data: {
             access: {restricted: false}
           }
         }).state("login", {
         params: {toState: "/home"},
         url: '/login',
         templateUrl: "/partials/login",
         controller: "loginCtrl",
         data: {
           access: {restricted: false}
         }
       }).state('support', {
         url: '/support',
         templateUrl: "/partials/support",
         data: {
           access: {restricted: false}
         }
       }).state("signup", {
         url: "/signup",
         templateUrl: "/partials/signup",
         controller: "signUpCtrl",
         data: {
           access: {restricted: false}
         }
       }).state("about", {
         url: '/about',
         templateUrl: "/partials/about",
         controller: "mainCtrl",
         data: {
           access: {restricted: false}
         }
       }).state("subscriptions", {
         url: '/subscriptions',
         templateUrl: "/partials/subscriptions",
         data: {
           access: {restricted: false}
         }
       }).state("contact", {
         url: '/contact',
         templateUrl: "/partials/contact",
         controller: "mainCtrl",
         data: {
           access: {restricted: false}
         }
       }).state("profile", {
         url: "/profile",
         templateUrl: "/partials/profile",
         controller: "userProfileCtrl",
         data: {
           access: {restricted: true}
         },
         resolve: {
           comments: function(UserProfileService){
             return UserProfileService.comments();
           },
           grades: function($http){
             return $http.get("/grades", function(response){
              if(response)
                return response.data;
              return null;
             });
           },
           uploadedCoins: function(){
             return null;
           }

         }
       }).state("setusername", {
         templateUrl: "/partials/setUsername",
         controller: "setUsernameCtrl",
         data: {
           access: {restricted: true}
         }
       }).state("forgotpassword", {
         url: "/forgotpassword",
         templateUrl: "/partials/forgotpassword",
         controller: "forgotPasswordCtrl",
         data: {
           access: {restricted: false}
         }
       }).state("resetpassword", {
         url: "/resetpassword/:token",
         templateUrl: "/partials/resetpassword",
         controller: "resetPasswordCtrl",
         resolve: {
           email: function($stateParams, AuthService){
             return AuthService.getEmailFromToken($stateParams.token);
           }
         },
         data: {
           access: {restricted: false}
         }
       }).state("termsofservice", {
         url: "/termsofservice",
         templateUrl: "/partials/tos",
         controller: "termsOfServiceCtrl",
         data: {
           access: {restricted: true}
         }
       }).state("coin-upload", {
         url: "/coin/upload",
         templateUrl: "/partials/coinUpload",
         controller: "coinUploadCtrl",
         resolve: {
           //TODO: move this to a service
           coinSeries: function($http){
             return $http.get("/coin-series/");
           },
         },
         data: {
           access: {restricted: true}
         }
       }).state("import-from-tpg", {
         url: "/coin/import",
         templateUrl: "/partials/import",
         controller: "importCtrl",
         data: {
           access: {restricted: true}
         }
       })
       .state("how-it-works-crowd-grade", {
         url: "/crowdgrade/howitworks",
         templateUrl: "/partials/howitworkscrowdgrade",
         data: {
           access: {restricted: false}
         }
       })
       .state("how-it-works-variety", {
         url: "/varietyeye/howitworks",
         templateUrl: "/partials/howitworksvariety",
         data: {
           access: {restricted: false}
         }
       }).state("disclaimer", {
         url: "/disclaimer",
         templateUrl: "/partials/disclaimer",
         data: {
           access: {restricted: false}
         }
       })
       //how-it-works-crowd-grade how-it-works-variety


       ;

});
