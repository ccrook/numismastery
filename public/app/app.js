angular.module("numismastery", ['ngResource', 'ngRoute', 'ngFileUpload', 'ui.slider', 'csl.taggableImage', 'csl.resizableDraggable',
 'csl.zoomableImage', 'ui.router', 'ngAnimate', 'ngTable', 'numismastery.filters', 'vcRecaptcha']);


angular.module("numismastery").run(function($rootScope, $window, $location, $timeout, $route, $state, $templateCache, AuthService){
  //Setup analytics callbacks
  $rootScope.$on('$viewContentLoaded', function(event) {
     $window.ga('send', 'pageview', { page: $location.path() });
  });


  //CPC: not sure if this is used
  $rootScope.$on("userUpdated", function(event, args){
    $timeout(function(){
      AuthService.getUser().then(function(refreshedUser){
        $timeout(function(){
          $rootScope.$apply(function(rootScope){
            rootScope.user = refreshedUser;
          });
        }, 0);
      });



    },0);

  });

  $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

    function redirect(user){
      console.log("redirect check", user);
      //Set the user
      $rootScope.user = user;
      //If protected route
      if (toState.data && toState.data.access.restricted && toState.data.access.restricted == true && !AuthService.isLoggedIn()) {
        event.preventDefault();
        console.log("TO STATE", toState);
        if(!toParams.toState || toState.name != "login"){
          return $state.go('login', {toState: toState});
        }

        //TODO: pass original state they were trying to go to so we can redirect
      }else if(user && !user.termsAcceptedDate && toState.name != "termsofservice" && toState.name != "setusername"){       //If not terms accepted
        event.preventDefault();
        console.log("TO TOS", toState.name);
        return $state.go('termsofservice');
      } else if(user && !user.displayName && toState.name != "setusername" && toState.name != "termsofservice"){ //If no username set
        event.preventDefault();
        console.log("TO set username");
        return $state.go('setusername');
      }else if(toState.name == "grade" && (!$rootScope.fields || ($rootScope.fields && !$rootScope.fields.selectedSeries))){ //if no selected series
        //event.preventDefault();
        return $state.go("start-grading");
      }


    };

    // if (toState.resolve) {
    //   console.log("LOADING TRUE");
    //   console.log(toState);
    $rootScope.loadingPromise = $timeout(function(){
      $rootScope.loading = true;
    }, 300);

    //}
    //Force redirect to setusername if one isn't set
    AuthService.getUser().then(redirect);


  });

  $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
    //  if (toState.resolve) {

          $rootScope.loadingPromise.then(function(){
            console.log("LOADING FALSE");
            $rootScope.loading = false;
          })

    //  }
  });

  $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
    $timeout(function(){
      console.log("STATE CHANGE ERROR");
      console.log(error);
      console.log(event);
      console.log(fromParams);
      $rootScope.errorMsg = error && error.data ? error.data : error;
      $state.go("error", { error: $rootScope.errorMsg }, {reload: true})
    })

  });
  //
  // $rootScope.redirectOnError = function(){
  //   console.log("rootscope errorMsg changed");
  //   //if(newValue != oldvalue){
  //     $timeout(function() {
  //       console.log("timeout call");
  //        $location.path($rootScope.errorRedirect.url);
  //      }, $rootScope.errorRedirect.delaySeconds * 1000);
  //
  //
  //   //}
  //
  // };

});
