module.exports = function(config){

  var paypal = require('paypal-rest-sdk');

  paypal.configure(config.paypal.api_config);

  return paypal;
};
