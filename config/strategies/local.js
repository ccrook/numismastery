var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var User = require('mongoose').model('User');
var bCrypt = require("bcrypt");

module.exports = function() {

  function isValidPassword(user, password){
    console.log("USER", user, "PASSWORD", password);
    console.log(bCrypt.compareSync(password, user.password));
    return bCrypt.compareSync(password, user.password);
  }

  passport.use(new LocalStrategy({
      passReqToCallback : true,
      usernameField: 'username',
      passwordField: 'password'
    },
    function(req, email, password, done) {
      console.log("LOCAL STRAT");
      // check in mongo if a user with username exists or not
      User.findOne({$and: [{ email :  email},
                          { 'accountConfirmation.code' : { $exists : false }},
                          { disabled: false }] },
        function(err, user) {
          console.log("LOCAL STRAT");
          console.log(err,user);
          // In case of any error, return using the done method
          if (err)
            return done(err);
          // Username does not exist, log error & redirect back
          if (!user){
            console.log('User Not Found with email ' + email);
            return done(null, false, "User not found.  Please make sure you have activated your account.");//,                  req.flash('message', 'User Not found.'));
          }
          //User exists but wrong password, log the error
          if (!isValidPassword(user, password)){
            console.log('Invalid Password');
            return done(null, false, email);//,                 req.flash('message', 'Invalid Password'));
          }
          // User and password both match, return user from
          // done method which will be treated like success
          return done(null, user);
        });
  }));

};
