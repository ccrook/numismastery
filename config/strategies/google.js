var passport = require('passport');
var url = require('url');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var config = require('../config');
var User = require('mongoose').model('User');

module.exports = function(app) {
  passport.use(new GoogleStrategy({
      clientID: config.google.clientID,
      clientSecret: config.google.clientSecret,
      callbackURL: config.google.callbackURL
    },
    function(accessToken, refreshToken, profile, done) {
      User.findOrCreate({ googleId: profile.id,
                          first_name: profile.name.givenName,
                          last_name: profile.name.familyName,
                          email: profile.emails[0].value},
      function (err, user) {
        //TODO: send welcome email if first login?
        //app.mailer.sendWelcome(user);
        return done(err, user);
      });
    }
  ));
};
