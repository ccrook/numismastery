var passport = require('passport');
var url = require('url');
var FacebookStrategy = require('passport-facebook').Strategy;
var config = require('../config');
var User = require('mongoose').model('User');

module.exports = function(app) {
  passport.use(new FacebookStrategy({
      clientID: config.facebook.clientID,
      clientSecret: config.facebook.clientSecret,
      callbackURL: config.facebook.callbackURL,
      enableProof: false,
      profileFields: ['id', 'displayName', 'first_name', 'last_name', 'emails']
    },
    function(accessToken, refreshToken, profile, done) {
      User.findOrCreate({
        facebookId: profile.id,
        first_name: profile.first_name,
        last_name: profile.last_name,
        email: profile.emails
       },
       function (err, user) {
        //TODO: send welcome email if first login?
        //app.mailer.sendWelcome(user);
        return done(err, user);
      });
    }
  ));
};
