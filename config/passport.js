var passport = require('passport');
var mongoose = require('mongoose');

module.exports = function(app) {
  var User = mongoose.model('User');

  passport.serializeUser(function(user, done) {
    var sessionData = {};
    console.log("IN SERIALIZE");
    sessionData.user = user._id;

    done(null, user);
  });

  // deserialize sessions
  passport.deserializeUser(function(sessionData, done) {
    console.log("IN DESERIALIZE");
    User.findOne({email: sessionData.email}, function (error, user) {
      if(error){
        //console.log(error);
        done(error);
      } else {
        //console.log(user);
        // mongoUser = user;
        done(null, user);
      }
    });
  });

  require('./strategies/local.js')();
  require('./strategies/facebook.js')(app);
  require('./strategies/google.js')(app);
};
