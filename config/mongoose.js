var config = require('./config');
var mongoose = require('mongoose');

module.exports = function() {
  var db = mongoose.connect(config.db);

  mongoose.connection.on('error',console.error.bind(console, "connection error..."));


  // Require model definition files here
  require('../server/models/User');
  require('../server/models/CoinGrades');
  require('../server/models/CoinSeries');
  require('../server/models/Comments');
  require('../server/models/GradeScale');
  require('../server/models/Coins.js');



// db.on("error", console.error.bind(console, "connection error..."));
  return db;
};
