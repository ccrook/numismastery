var config = require('./config');
var express = require('express');
var bodyParser = require('body-parser');
var cookieparser = require('cookie-parser');
var session = require('express-session');
var passport = require('passport');
var stylus = require('stylus');
var path = require('path');


module.exports = function() {
  var app = express();
  //Setup logging with Morgan
  if (process.env.NODE_ENV === 'development') {
    app.use(require('morgan')('dev'));
  }


  //setup bodyparser
  app.use(bodyParser.json());

  //Stylus for serving up CSS
  app.use(stylus.middleware({
    src: __dirname + '/public',
    compile: compile
  }));

  function compile(atr, path){
    return stylus(atr).set('filename', path);
  }
  // What's this?
  //app.use(errorHandler({ dumpExceptions:true, showStack:true }));

  //Set view enginge to jade
  app.set('views', './server/views');
  app.set('view engine', 'jade');

  // Cookie parser setup
  app.use(cookieparser('myBigSecret'));
  // console.log(env);
  // console.log(config.get(env).cookie.domain);

  // Session setup
  app.use(session({
     secret  : 'myBigSecret',
    //  cookie  : {
    //    expires: false,
    //    domain: env == "dev" ? "localhost" : config.get(env).cookie.domain,
    //    secure: false
    //  },
     resave: false,
    //  store: new MongoStore({ mongooseConnection: db },
    //    function(error) {
    //         if(error) {
    //             return console.error('Failed connecting mongostore for storing session data. %s', error.stack);
    //         }
    //         return console.log('Connected mongostore for storing session data');
    //     }),
     saveUninitialized: true
     //store: redisSessionStore
   }));

   //Static routing for matches in public
   var oneDay = 86400000;
   //app.use(express.compress());
   app.use(express.static('public', { maxAge: oneDay }));
   //
   // app.get('/login', function(req, res){
   //   if(req.user){
   //     res.redirect("/");
   //   }else{
   //     res.render("partials/login");
   //   }
   //
   // });
   // Passport
   app.use(passport.initialize());
   app.use(passport.session());

   /* SSL redirect on prod At the top, with other redirect methods before other routes */
  app.get('*',function(req,res,next){
    var baseUrl = config.env == "development" ? "localhost:" + process.env.PORT : "www.numismastery.com";
    if(config.env == "production"){
      if(req.headers['x-forwarded-proto']!='https')
        res.redirect('https://' + baseUrl + req.url);
      else
        next(); /* Continue to other routes if we're not redirecting */
    }else{
      next();
    }

  });


   app.use(require("../server/services/mailer")(app));

    //API routes
    require('../server/routes/api/auth.js')(app);
    require('../server/routes/api/subscriptions.js')(app, config);
    require('../server/routes/api/user.js')(app);
    require('../server/routes/api/import.js')(app);


   // Front end routes
   require('../server/routes/authentication')(app);
   require('../server/routes/coin-series.js')(app);
   require('../server/routes/grades.js')(app);
   require('../server/routes/upload.js')(app);
   require('../server/routes/coin.js')(app);


   app.get("/", function(req, res){
     res.render("splash");
   });

   //Jade for angular partials
   app.get('/partials/:partialPath', function(req, res){
     res.render("partials/" + req.params.partialPath, {
       User: req.user
     });
   });

   app.get("*", function(req, res){
     console.log(req.user);
     res.render("index", {
       User: req.user
     });
     // console.log("IMAGES");
     // console.log(images);

   });

  return app;
};
