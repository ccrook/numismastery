module.exports = function(){

  var copperBillingPlan = {
      "description": "Copper Subscription Plan",
      "merchant_preferences": {
          "auto_bill_amount": "yes",
          "cancel_url": "https://www.numismastery.com/profile",
          "initial_fail_amount_action": "continue",
          "max_fail_attempts": "1",
          "return_url": "https://www.numismastery.com/subscribed"
      },
      "name": "Testing-CopperPlan",
      "payment_definitions": [
          {
              "amount": {
                  "currency": "USD",
                  "value": "10"
              },
              "cycles": "0",
              "frequency": "MONTH",
              "frequency_interval": "1",
              "name": "Regular 1",
              "type": "REGULAR"
          }
      ],
      "type": "INFINITE"
  };

  var copperBillingAgreement = function(isoDate){
    return {
      "name": "Numismastery Copper-Level Agreement",
      "description": "Here's where another version of my ToS would go.",
      "start_date": isoDate,
      "plan": {
          "id": "P-0NJ10521L3680291SOAQIVTQ"
      },
      "payer": {
          "payment_method": "paypal"
      },
    };
  };


    var activateBillingPlan = [
        {
            "op": "replace",
            "path": "/",
            "value": {
                "state": "ACTIVE"
            }
        }
    ];

  return {
    copperBillingPlan: copperBillingPlan,
    copperBillingAgreement: copperBillingAgreement,
    activateBillingPlan: activateBillingPlan
  };

};
