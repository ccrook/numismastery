var config = require("../../config/config.js");

var templates = {
  welcome: "aca8a28e-719e-484b-9392-f8fddfe8eebf",
  account_confirmation: "2f137ee5-afbf-4df7-aee3-8433e901ebd0",
  password_reset_token: "7f7e2f80-5e0e-49ec-969a-00bdf525c29c",
  //password_reset_success: "",
  subscription_activated: "",
  subscription_deactivated: ""
};

function sendViaTemplate(to, templateId, subs){
  var sendgrid = require("sendgrid")(config.sendgrid.api_key);
  var email = new sendgrid.Email();
  email.addTo(to);
  email.subject = "Numismastery";
  email.from = config.mail.addresses.noreply;
  email.text = ' ';
  email.html = ' ';

  if(subs){
    subs["-host-"] = config.env == "development" ? "localhost:" + process.env.PORT : "www.numismastery.com";
    var keys = Object.keys(subs);
    for(var i=0;i<keys.length;i++){
      var key = keys[i];
      email.addSubstitution(key,subs[key]);
    }
  }

  // or set a filter using an object literal.
  email.setFilters({
      'templates': {
          'settings': {
              'enable': 1,
              'template_id' : templateId,
          }
      }
  });
  console.log("SENDING", email);
  sendgrid.send(email);
}

function sendAccountConfirmation(user){
  var subs = {
    //from_this : to_that
    '-code-': user.accountConfirmation.code
  };
  sendViaTemplate(user.email, templates.account_confirmation, subs);
}

function sendWelcome(user){
  var subs = {
    //from_this : to_that
  };
  sendViaTemplate(user.email, templates.welcome, subs);
}

function sendPasswordReset(user){
  var subs = {
    //from_this : to_that
    '-token-': user.reset_token
  };
  sendViaTemplate(user.email, templates.password_reset_token, subs);
}

module.exports = function(app){
  var mailer = {
    sendViaTemplate: sendViaTemplate,
    sendAccountConfirmation: sendAccountConfirmation,
    sendWelcome: sendWelcome,
    sendPasswordReset: sendPasswordReset,
    test: function(){
      sendViaTemplate("ccrook@gmail.com", "aca8a28e-719e-484b-9392-f8fddfe8eebf");
    }
  };
  //hack so this is always available
  app.mailer = mailer;
  //make it available off request too middleware
  return function(req, res, next){
    req.mailer = mailer;
    next();
  };
};
