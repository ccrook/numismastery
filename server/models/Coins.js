var mongoose = require('mongoose');

var coinsSchema = new mongoose.Schema({
  pcgs_number: {type: String},
  cert_number: {type: String},
  date_mintmark: {type: String},
  year: Number,
  mintmark: String,
  denomination: {type: String},
  country: {type: String},
  grade: {type: String},
  image_url: {type: String},
  series: {type: mongoose.Schema.Types.ObjectId, ref: 'CoinSeries'},
  images: {
    "obverse": [],
    "reverse": [],
    "edge": []
  },
  gradedBy: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
  comments: [{type: mongoose.Schema.Types.ObjectId, ref: 'CoinComments'}],
  grades: [{type: mongoose.Schema.Types.ObjectId, ref: 'CoinGrades'}],
  stats: {
    sumOfUserGrades: {type: Number},
    averageUserGradeValue: {type: Number},
    stdDev: {type: Number},
    gradeCount: {type: Number},
    sumOfSquares: {type: Number},
    averageUserGrade: {type: Number}//{type: mongoose.Schema.ObjectId, ref: 'GradeScale'}
  },
  dateCreated: { type: Date, default: Date.now },
  uploadedBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},

  //TODO: problem coin designations

  //Whether or not the coin as been approved or not (to allow us to remove offending coins)
  approved: { type: Boolean, default: true },
  dateReview: Date,

  //Log of people reporting comment
  reports: [{
    reportedBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    dateReported: {type: Date, default: Date.now() },
    reason: String,
    comments: String
  }],

  //Whether or not the coin is a featured coin
  featured: Boolean,


}, { collection: 'Coins' });


//TODO: pull into util
var paginate = function(query, params, callback){
  // query.count(function(err, count){
  //   totalCount = count;
  // });

  var total;
  query.count().exec(function(err, total){
    total = total;
    query.find();
    console.log(params);
    query
      .limit(params.perPage)
      .skip(params.perPage * (params.page-1))
      //.sort() //TODO: actually implement this via ngTable
      .exec(function(err, coins) {
        console.log(total);
        callback(err, {
          total: total,
          results: coins
        });

        //console.log(query);

      });
  });


};

coinsSchema.methods.reportCoin = function(reason, comments, currentUser, callback){
  this.reports.push({
    reportedBy: currentUser,
    reason: reason,
    comments: comments
  });
  this.save(callback);
};

coinsSchema.methods.unreportCoin = function(currentUser, callback){
  return this.reports.pull({reportedBy: currentUser}, callback);
};

coinsSchema.methods.getComments = function(params, callback){
  //var query = this.model("Coins").findById(this._id).populate("comments").select('comments');
  var query = this.model('CoinComments').find({"coin": this._id }).populate("postedBy related_to_grade grade");
  paginate(query, params, callback);
};

coinsSchema.methods.addComment = function(commentToAdd, callback){
  commentToAdd.coin = this;
  return commentToAdd.save(callback);
  //this.save(callback);""
  //return this.model('CoinComments').find({ coin: this._id }, callback);
};

coinsSchema.methods.deleteComment = function(commentToDelete, callback){

  this.model('CoinComments').remove(commentToDelete).exec(callback);
};

coinsSchema.statics.getUngradedCoinInSeries = function(series, filters, currentUser, callback){
  var Coins = this.model("Coins");
  var CoinGrades = this.model('CoinGrades');
  console.log("CURRENT USER", currentUser);

  Coins.findOne({
     $and : [
      {'pcgs_number': { $in: series.pcgs_numbers}},
      {'gradedBy': { $not: { $elemMatch: { $eq: currentUser._id }}}},
      {uploadedBy: { $ne: currentUser._id }},
      {'image_url':{'$regex' : '^((?!noimg).)*$', '$options' : 'i'}},
      //{ $or : [{'approved' : true }, {'approved': {$exists: false}}]}, //only pull back approved coins
      {'reports' : { $exists: false }} //only pul lback unreported coins
    ] //prevent grading own coin
  }, callback);

};

coinsSchema.methods.approve = function(callback){
  this.approved = true;
  this.save(callback);
};

coinsSchema.methods.unapprove = function(callback){
  this.approved = false;
  this.save(callback);
};

coinsSchema.methods.getGrades = function(callback){
  this.model('CoinComments').find({gradedBy: {$elemMatch: { $eq: currentUser._id }}}, callback);
};


coinsSchema.methods.addGrade = function(gradeToAdd, callback){

  this.gradedBy.push(gradeToAdd.gradedBy);
  this.grades.push(gradeToAdd);

  console.log("STATS", this.stats);
  //Calculate user stats
  if(!this.stats){
    this.stats = {};
  }
  if(!this.stats.gradeCount){
    console.log("setting grade count to zero");
    this.stats.gradeCount = 0;
  }
  if(!this.stats.sumOfUserGrades){
    console.log("setting sum of user grades count to zero");
    this.stats.sumOfUserGrades = 0;
  }
  if(!this.stats.sumOfSquares){
    console.log("setting sum of squares to zero");
    this.stats.sumOfSquares = 0;
  }

  this.stats.gradeCount++;
  this.stats.sumOfUserGrades+=gradeToAdd.grade.value;
  this.stats.averageUserGradeValue = Math.floor(this.stats.sumOfUserGrades/this.stats.gradeCount);


  this.stats.sumOfSquares = this.stats.sumOfSquares + Math.pow(gradeToAdd.grade.value,2);
  var variance = ((this.stats.sumOfSquares - (Math.pow(this.stats.sumOfUserGrades,2)/this.stats.gradeCount)) / this.stats.gradeCount);
  //var sdSum = 0;
  //sdSum+= (parseInt(result[0].columns[column].value) - averageUserGrade)^2
  this.stats.stdDev = this.stats.gradeCount == 1 ? 0 : Math.sqrt(variance);

  this.save(callback);

};

coinsSchema.statics.gradedByUserSince = function(user, sinceDateTime, callback){
  this.model('CoinGrades').find({dateGraded: { $gt: sinceDateTime }, gradedBy: user}, callback);
};

module.exports = mongoose.model("Coins", coinsSchema);

/*     if(result.length  > 0){
//           var statsAlreadyExist = Object.keys(result[0].columns).filter(function(column){
//             if(column == "stats:sumOfUserGrades" || column == "stats:averageUserGrade" || column == "stats:standardDeviationOfUserGrades" || column == "stats:countOfUserGrades"){
//               //TODO: this probably isn't accurate
//               return true;
//             }
//           });
//
//           var sdSum = 0;
//           if(statsAlreadyExist){
//             console.log("stats already exist");
//             gradeCount = result[0].columns["stats:countOfUserGrades"] && isFinite(result[0].columns["stats:countOfUserGrades"].value) ? result[0].columns["stats:countOfUserGrades"].value : 0;
//             gradeSum = result[0].columns["stats:sumOfUserGrades"] && isFinite(result[0].columns["stats:sumOfUserGrades"].value) ? result[0].columns["stats:sumOfUserGrades"].value + parseInt(userGrade) : parseInt(userGrade);
//             sumOfSquares = result[0].columns["stats:sumOfSquares"] && isFinite(result[0].columns["stats:sumOfSquares"].value) ? result[0].columns["stats:sumOfSquares"].value + parseInt(userGrade)^2 : parseInt(userGrade)^2;
//             gradeCount++;
//             averageUserGrade = gradeSum/gradeCount;
//             var variance = ((sumOfSquares - (gradeSum^2/gradeCount)) / gradeCount);
//             // for(column in result[0].columns){
//             //   if(column.indexOf("countOfUserGrades") < 0 && column.indexOf("sumOfUserGrades") < 0 && column.indexOf("standardDeviationOfUserGrades") < 0 && column.indexOf("averageUserGrade") < 0){
//             //     sdSum+= (parseInt(result[0].columns[column].value) - averageUserGrade)^2;
//             //   }//if
//             // }//for
//
//             stdDev = gradeCount == 1 ? 0 : Math.sqrt(sdSum/gradeCount);
//             console.log("STATS");
//             console.log(gradeCount, gradeSum, averageUserGrade, stdDev, sumOfSquares, variance);
//           }else{
//             for(column in result[0].columns){
//               if(column.indexOf("user_grades:") == 0){
//                 gradeCount++;
//                 gradeSum+=parseInt(result[0].columns[column].value);
//                 sumOfSquares+=parseInt(result[0].columns[column].value)^2;
//               }
//               if(column.indexOf("user_grades:" + req.user.email) == 0){
//                 returnValue.userGrade = result[0].columns[column].value;
//               }//if user grades
//             }//for
//             averageUserGrade = (gradeSum + parseInt(userGrade)) / (gradeCount++);
//             // for(column in result[0].columns){
//             //   console.log(result[0].columns[column]);
//             //   if(column.indexOf("user_grades") == 0){
//             //     sdSum+= (parseInt(result[0].columns[column].value) - returnValue.averageUserGrade)^2;
//             //   }//if
//             // }//for
//             var variance = ((sumOfSquares - (gradeSum^2/gradeCount)) / gradeCount);
//             console.log(gradeCount, gradeSum, averageUserGrade, stdDev, sumOfSquares, variance);
//             stdDev = gradeCount == 1 ? 0 : Math.sqrt(sdSum/count);
//           }//else
//
//         }//if
//         client.mutateRow("Numismastery", certNumber, [
//           new HBaseTypes.Mutation({column: "user_grades:" + req.user.email, value: userGrade.toString()}),
//           new HBaseTypes.Mutation({column: "stats:" + "sumOfUserGrades", value: gradeSum.toString()}),
//           new HBaseTypes.Mutation({column: "stats:" + "averageUserGrade", value: averageUserGrade.toString()}),
//           new HBaseTypes.Mutation({column: "stats:" + "standardDeviationOfUserGrades", value: stdDev.toString()}),
//           new HBaseTypes.Mutation({column: "stats:" + "countOfUserGrades", value: gradeCount.toString()}),
//           new HBaseTypes.Mutation({column: "stats:" + "sumOfSquares", value: sumOfSquares.toString()})

*/
