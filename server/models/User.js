var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bCrypt = require("bcrypt");
var findOrCreate = require('mongoose-findorcreate');


var userSchema = new Schema({
  googleId: { type: String, default: undefined },
  facebookId: { type: String, default: undefined },
  email: {type: String },
  username: {type: String},//, required: [true, 'Display name is required.']}, //TODO: is this used?
  password: {type: String },//, required: [true, 'Password is required.']},
  first_name: {type: String },
  last_name: {type: String },
  displayName: { type: String, default: undefined, unique: true },
  subscriptionId: String,
  lastRecaptcha: Date,
  reset_token: String,
  coinCredits: { type: Number, default: 25 }, //By default give user one coin when signing up
  termsAcceptedDate: {type: Date },
  disabled: {type: Boolean, default: false },
  accountConfirmation: {
    code: String,
    dateConfirmed: Date
  },
  subscription: String //TODO: what does payment actually provide?
}, { collection: 'Users' });

userSchema.plugin(findOrCreate);
//

var paginate = function(query, params, callback){
  // query.count(function(err, count){
  //   totalCount = count;
  // });

  var total;
  query.count().exec(function(err, total){
    total = total;
    query.find();
    console.log(params);
    query
      .limit(params.perPage)
      .skip(params.perPage * (params.page-1))
      //.sort() //TODO: actually implement this via ngTable
      .exec(function(err, coins) {
        console.log(total);
        callback(err, {
          total: total,
          results: coins
        });

        //console.log(query);

      });
  });


};

function createHash(password){
  return bCrypt.hashSync(password,  bCrypt.genSaltSync(10));
}

function isValidPassword(user, password){
  return bCrypt.compareSync(password, user.password);
}

//***INSTANCE METHODS****
userSchema.methods.getUploadedCoins = function(params, callback){

  var query = this.model('Coins').find({uploadedBy: this._id}).populate('series');

  paginate(query, params, callback);

};

userSchema.methods.getComments = function(params, callback){
  var query = this.model('CoinComments').find({postedBy: this._id}).populate('related_to_grade postedBy');

  paginate(query, params, callback);

};

userSchema.methods.getGradedCoins = function(params, callback){
  var query = this.model('CoinGrades').find({gradedBy: this._id}).populate('coin grade');//.exec(callback);

  paginate(query, params, callback);
};

userSchema.methods.gradesSinceLastRecaptcha = function(callback){
  this.model('Coins').gradedByUserSince(this, this.lastRecaptcha, callback);
};

userSchema.methods.gradesInLastXMinutes = function(minutes, callback){
  var MS_PER_MINUTE = 60000;
  this.model('Coins').gradedByUserSince(this, Date.now() - minutes * MS_PER_MINUTE, callback);
};

userSchema.methods.setPassword = function(newPassword, callback){
  //TODO: check to make sure token is valid

  this.password = createHash(newPassword);
  this.save(function(err){
    if(callback && typeof(callback) == "function")
      callback(err);
  });
};


userSchema.methods.enable = function(callack){
  this.disabled = false;
  this.save(function(err, updated){
    if(callback && typeof(callback) == "function")
      callback(err, updated);
  });
};

userSchema.methods.disable = function(callback){
  this.disabled = true;
  this.save(function(err, updated){
    if(callback && typeof(callback) == "function")
      callback(err, updated);
  });
};

userSchema.methods.acceptTerms = function(callback){
  this.termsAcceptedDate = Date.now();
  this.save(function(err, updated){
    if(callback && typeof(callback) == "function")
      callback(err, updated);
  });
};


userSchema.methods.generateResetToken = function(callback){
  var md5 = require('md5');
  this.reset_token = md5(this._id + Date.now().toString());
  console.log(this.reset_token);
  var user = this;
  this.save(function(err){
    if(callback && typeof(callback) == "function")
      callback(err, user);
  });

};

userSchema.methods.generateConfirmationCode = function(callback){
  var md5 = require('md5');
  this.accountConfirmation.code = md5(this._id + Date.now().toString());
  console.log(this.reset_token);
  var user = this;
  this.save(function(err){
    if(callback && typeof(callback) == "function")
      callback(err, user);
  });

};

userSchema.methods.giveCredits = function(creditCount, callback){
  user.coinCredits += creditCount;
  user.save(callback);
};

//****STATICS*****

userSchema.statics.register = function(user, callback){
  user.save(function(err, user){
    if(err){
      callback(err, null);
    }
    callback(err, user);
  });
};

userSchema.statics.getByToken = function(token, callback){
  this.findOne({reset_token: token}, function(err, user){
    if(callback && typeof(callback)=="function"){
      callback(err, user);
    }
  });
};

userSchema.statics.getByConfirmationCode = function(token, callback){
  this.findOne({'accountConfirmation.code': token}, function(err, user){
    if(callback && typeof(callback)=="function"){
      callback(err, user);
    }
  });
};


module.exports = mongoose.model('User', userSchema);
