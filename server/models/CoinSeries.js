var mongoose = require('mongoose');

var coinSeriesSchema = mongoose.Schema({
  series: String,
  pcgs_numbers: Array,
  proof: Boolean,
  suffixes: []
}, { collection: 'CoinSeries' });

module.exports = mongoose.model("CoinSeries", coinSeriesSchema);
