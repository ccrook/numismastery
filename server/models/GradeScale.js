var mongoose = require('mongoose');

var gradeScaleSchema = new mongoose.Schema({
  grade: String,
  value: Number,
  description: String,
  abbrev: String,
  number: String,
  text: String
}, { collection: 'GradeScale' });

module.exports = mongoose.model("GradeScale", gradeScaleSchema);
