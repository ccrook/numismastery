var mongoose = require('mongoose');

var commentSchema = new mongoose.Schema({
    postedBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    dateCreated: { type: Date, default: Date.now },
    body: String,
    coin: {type: mongoose.Schema.Types.ObjectId, ref: 'Coins'},

    //Track which CoinGrade this was submitted along with
    related_to_grade: {type: mongoose.Schema.Types.ObjectId, ref: 'CoinGrades'},
    //Track the actual grade since we can't populate nested documents
    grade: {type: mongoose.Schema.Types.ObjectId, ref: 'GradeScale'},
    //Voting (just track user who voted)
    votes: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],

    //Tracking parent/child relationships
    parent: {type: mongoose.Schema.Types.ObjectId, ref: 'CoinComments'},
    responses: [{type: mongoose.Schema.Types.ObjectId, ref: 'CoinComments'}],

    //Log of people reporting comment
    reports: [{
      reportedBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
      dateReported: Date,
      reason: String,
      comments: String
    }]

});

commentSchema.statics.getByCoinId = function(coinId, callback){
  return this.model('CoinComments').find({postedBy: coinId}, callback);
};

commentSchema.methods.reportComment = function(reason, comments, currentUser, callback){
  return this.reports.push({
    reportedBy: currentUser,
    reason: reason,
    comments: comments
  }).save(callback);
};

commentSchema.methods.unreportCoin = function(currentUser, callback){
  return this.reports.pull({reportedBy: currentUser}, callback);
};


module.exports = mongoose.model("CoinComments", commentSchema, "CoinComments");
