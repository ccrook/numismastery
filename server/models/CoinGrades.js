var mongoose = require('mongoose');

var coinGradeSchema = new mongoose.Schema({
  coin: {type: mongoose.Schema.Types.ObjectId, ref: 'Coins'},
  gradedBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  grade: {type: mongoose.Schema.Types.ObjectId, ref: 'GradeScale'},
  dateGraded: { type: Date, default: Date.now }
}, { collection: 'CoinGrades' });

module.exports = mongoose.model("CoinGrades", coinGradeSchema);
