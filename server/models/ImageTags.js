var mongoose = require('mongoose');

var imageTagSchema = new mongoose.Schema({
    postedBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    dateCreated: { type: Date, default: Date.now },
    comment: String,
    coin: {type: mongoose.Schema.Types.ObjectId, ref: 'Coins'},

    //Track which grade this was submitted along with
    related_to_grade: {type: mongoose.Schema.Types.ObjectId, ref: 'CoinGrades'},

    //Position on the image, center of bounding box
    position: {
      topLeft: Number,
      topRight: Number,
      bottomLeft: Number,
      bottomRight: Number
    },

    //Tag type, i.e. pickup point, damage, etc.
    type: String
});

module.exports = mongoose.model("ImageTags", imageTagSchema, "ImageTags");
