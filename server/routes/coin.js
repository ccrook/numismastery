var express = require('express')
  // , router = express.Router()
  , mongoose = require('mongoose')
  , ObjectId = mongoose.Schema.ObjectId
  , config = require('../../config/config');

module.exports = function(app) {
  var db = mongoose.connection;


  var Coins = require('../models/Coins');
  var CoinSeries = require('../models/CoinSeries');
  var GradeScale = require("../models/GradeScale");
  var CoinGrades = require('../models/CoinGrades');
  var CoinComments = require('../models/Comments');


  function checkIfRecaptchaRequired(req, res, callback){
    if(req.user){
      var threshold = config.grading.recaptchaThreshold;//Configure this in the config so test env can run many many gradings
      //req.user.gradesInLastXMinutes(1, function(err, gradesInLastMinute){
        // console.log("GRADES IN LAST MINUTE " + gradesInLastMinute.length);
        // if(gradesInLastMinute.length >= threshold){

        //CPC: commented out previous code -- making recaptcha required after threshold, always
          req.user.gradesSinceLastRecaptcha(function(err, gradesSinceLastRecaptcha){
            console.log("SINCE LAST RECAPTCHA",gradesSinceLastRecaptcha.length);
             callback(gradesSinceLastRecaptcha.length >= threshold);
          });
        // }else{
        //   callback(gradesInLastMinute.length >= threshold);
        // }

      //});

    }else{
      callback(false);
    }

  }

  app.post("/coin", function(req, res){
    if(req.user){
      var coinId = req.body.coinId;

      //var certNumber = req.body.cert_number;
      var userGrade = req.body.selectedUserGrade.toString();
      //TODO: instead find by ID
      Coins.findById(coinId, function(err, coinRecord){

        if(!coinRecord || err){
          res.status(400).send("Error: Coin not found.");
          return;
        }

        //TODO: check if user has already graded this coin.  If so, throw an error
        CoinGrades.findOne({gradedBy: req.user, coin: coinRecord}, function(err, existingGrade){
          console.log(existingGrade);
          if(existingGrade && !err){
            //set status properly that this is a Bad Request
            //TODO: if user has already graded coin, delete the existing grade first
            res.status(400).send("Error: User already graded this coin.");
          }else{
            console.log(userGrade);
            var coinGradeRecord = new CoinGrades();

            var comment = new CoinComments();
            comment.related_to_grade = coinGradeRecord;
            comment.postedBy = req.user;
            comment.body = req.body.Comments;
            comment.coin = coinRecord;

            //TODO: get grade record by value
            GradeScale.findOne({value: userGrade}, function(err, grade){

                console.log(req.user);
                console.log(grade);
                if(err){
                  res.send(err);
                  res.end();
                }
                coinGradeRecord.grade = grade;
                coinGradeRecord.gradedBy = req.user._id;
                coinGradeRecord.coin = coinRecord;
                coinGradeRecord.save();
                comment.grade = grade;

                coinRecord.addGrade(coinGradeRecord, function(err, updatedCoin){
                    if(err){
                      res.status(500).send(err);

                    }
                    //Only save comment if it exists
                    if(req.body.Comments && req.body.Comments.length > 0){
                      comment.save();
                    }
                    res.send(coinGradeRecord);
                });


                //TODO: also write the ID of the coinGradeRecord to the actual record of the Coins table
                //TODO: write the comment (if any) to the CoinGrade table
                //TODO: write the comment to the user table (so we can pull out all user comments)
                //TODO: write the coinGradeRecord to the User table so we can track the number of grades a user has done
                //TODO: write the coinGradeRecord reference to the User table under the specific series the user has graded for the coin (so we can pull who is an expert on each

              //TODO: tabulate stats about the coin

            });
          }
        });
      });
    }else{
      res.status(403).json("You must be logged in to grade coins.");
    }

 });



  app.get("/coin/:coinId/grading-results", function(req, res){
    var coinId = req.params.coinId;
    console.log("GETTING RESULTS FOR: " + coinId);
    //TODO: update this to handle user-uploaded coins
    Coins.findOne({_id: coinId}).populate("stats.averageUserGrade").lean().exec(function(err, coinRecord){
      console.log(coinRecord);
      if(!coinRecord || err){
        res.status(400).send("Error: Coin not found.");
      }
      else{

        //try to find and inject the coin series
        if(coinRecord.uploadedBy && coinRecord.uploadedBy !== null){
          CoinSeries.findOne(coinRecord.series, function(err, series){
            coinRecord.series = series.series;
          });
        }

        if(req.user){
          console.log("USER FOUND");
          CoinGrades.findOne({ $and: [{ coin: coinId },
                                      {gradedBy: { $in : [req.user._id] }} ]}).populate("grade").exec(function(err, userGrade){
                                        console.log(userGrade);
            if(userGrade && userGrade !== null){
              coinRecord.userGrade = userGrade.grade;
              console.log(err, userGrade);
            }

            res.send(coinRecord);
            //res.send(userGrade);

          });
        }else{
          res.send(coinRecord);
        }


      }
    });
  });

  app.get("/coinFromSeries/:series", function(req, res){
    if(req.user && req.user !== null){
      return checkIfRecaptchaRequired(req, res, function(required){
        console.log("REQUIRED", required);
        if(required){
          return res.json(required);
        }


        var series = req.params.series;
        return CoinSeries.findOne({series: series}, function(err, coinSeries){
          var noMoreCoinsMsg = "Series not found.";


          if(err){
            return res.status(500).send(err);
          }
          else if(coinSeries === null){
            return res.status(500).send(noMoreCoinsMsg);
          }
          else if(!err){
            if(coinSeries && typeof(coinSeries.pcgs_numbers[0]) == "object"){
              coinSeries.pcgs_numbers = coinSeries.pcgs_numbers.map(function(obj){
                return obj.pcgs_number;
              });
              console.log(coinSeries);
            }
            return Coins.getUngradedCoinInSeries(coinSeries, {}, req.user, function(err, response){
              if(err){
                return res.status(500).send(err);
              }
              console.log(response);
              if(response === null){
                return res.status(200).send();
              }
              return res.send(response);
            });
           }

        });
      });
    }else{
      res.status(403).json("You must be logged in to perform this function.");
    }



  });

  //Report coin
  app.post("/coin/:coinId/report", function(req, res){
    if(req.user && req.user !== null){
      Coins.findById(req.params.coinId, function(err, foundCoin){
        if(err) return res.status(500).json(err);
        if(!foundCoin || foundCoin === null) return res.status(404).json("Coin not found.");


        var reason = req.body.reason;
        var comments = req.body.comments;
        var currentUser = req.user;
        return foundCoin.reportCoin(reason, comments, currentUser, function(err, savedCoin){
          if(err) return res.status(500).json(err);

          return res.status(200).send();
        });
      });
    }else{
      return res.status(403).json("You must be logged in to perform this function.");
    }
  });

  //Unreport coin
  app.delete("/coin/:coinId", function(req, res){
    if(req.user && req.user !== null){
      Coins.findById(req.params.coinId, function(err, removedCoin){
        if(err) return res.status(500).json(err);
        if(!removedCoin || removedCoin === null) return res.status(404).json("Coin not found.");
        console.log("REMOVED", removedCoin.uploadedBy.toString(), req.user._id.toString(), (req.user._id.toString() == removedCoin.uploadedBy.toString()));

        if(removedCoin.uploadedBy.toString() == req.user._id.toString()){
          return removedCoin.remove(function(err){
            if(err) return res.status(500).json(err);

            //If the coin hasn't been graded yet, refund the credits involved.
            if(removedCoin.grades.length === 0){
              req.user.coinCredits++;
              req.user.save();
            }
            return res.status(200).send("Coin successfully deleted.");
          });
        }
        return res.status(403).send("You may only delete coins you have uploaded.");


      });
    }else{
      res.status(401).json("You must be logged in to perform this function.");
    }
  });

  //Unreport coin
  app.delete("/coin/:coinId/report", function(req, res){
    if(req.user && req.user !== null){
      Coins.findById(req.params.coinId, function(err, foundCoin){
        if(err) return res.status(500).json(err);
        if(!foundCoin || foundCoin === null) return res.status(404).json("Coin not found.");

        foundCoin.unreport(req.user, function(err, savedCoin){
          if(err) return res.status(500).json(err);

          res.status(200).send();
        });
      });
    }else{
      res.status(403).json("You must be logged in to perform this function.");
    }
  });

  //Report comment
  app.post("/coin/:coinId/comment/:commentId/report", function(req, res){
    if(req.user && req.user !== null){
      // Coins.findById(req.params.coinId, function(err, foundCoin){
      //   if(err) return res.status(500).json(err);
      //   if(!foundCoin || foundCoin === null) return res.status(404).json("Coin not found.");
      //
      //
      //   var reason = req.body.reason;
      //   var comments = req.body.comments;
      //   var currentUser = req.user;
      //   foundCoin.reportCoin(reason, comments, currentUser, function(err, savedCoin){
      //     if(err) return res.status(500).json(err);
      //
      //     res.status(200).send();
      //   })
      // });
    }else{
      res.status(403).json("You must be logged in to perform this function.");
    }
  });

  //Unreport comment
  app.delete("/coin/:coinId/:commentId/report", function(req, res){
    if(req.user && req.user !== null){
      // Coins.findById(req.params.coinId, function(err, foundCoin){
      //   if(err) return res.status(500).json(err);
      //   if(!foundCoin || foundCoin === null) return res.status(404).json("Coin not found.");
      //
      //   foundCoin.unreport(req.user, function(err, savedCoin){
      //     if(err) return res.status(500).json(err);
      //
      //     res.status(200).send();
      //   });
      // });
    }else{
      res.status(403).json("You must be logged in to perform this function.");
    }
  });


  var extractParams = function(req){
    console.log(req.param);
    var perPage = parseInt(req.params.perPage) || 10;
    var page = Math.max(1, (parseInt(req.params.page) || 1));


    return {
      page: page,
      perPage: perPage
    };
  };

  function filterUserForComments(user){
    return {
      _id: user._id,
      displayName: user.displayName
    };
  }

  app.get("/api/coin/:coinId/comments", function(req,res){
    //if(req.user && req.user !== null){
      var params = extractParams(req);

      Coins.findById(req.params.coinId, function(err, foundCoin){
        if(err) return res.status(500).json(err);
        if(!foundCoin || foundCoin === null) return res.status(404).json("Coin not found.");

        foundCoin.getComments(params, function(err, populatedComments){
          if(err) return res.status(500).json(err);
          //TODO: filter the populated comments so email/first/last/etc arent' returned
          console.log(populatedComments.results);
          for(var i=0;i<populatedComments.results.length;i++){
            populatedComments.results[i].postedBy = filterUserForComments(populatedComments.results[i].postedBy);
            populatedComments.results[i].postedBy.coinCredits = undefined;
            populatedComments.results[i].postedBy.disabled = undefined;
            populatedComments.results[i].postedBy.accountConfirmation = undefined;
            console.log(populatedComments.results[i].postedBy);
          }

          res.json(populatedComments);
        });
      });

    //}
    // else{
    //   res.status(403).send("Access denied.  You must be logged in.");
    // }
  });


};
