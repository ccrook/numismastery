var AWS = require('aws-sdk');

module.exports = function(app) {
  app.post('/upload', function(request, response) {
    if(!request.user || request.user === null) return response.status(403).json({status: "error", errors: ["Access denied."]});
    if(request.user && request.user.coinCredits === 0) return response.status(400).json({status: "error", errors: ["Current user does not have enough coin credits to perform this action."]});



    //coinGradeRecord.uploaded
    var mongoose = require("mongoose");
    var CoinSeries = mongoose.model('CoinSeries');
    var Coins = mongoose.model('Coins');

    var formidable = require('formidable');
    var form = new formidable.IncomingForm();
   //form.encoding = "multipart/form-data";
   form.keepExtensions = true;
   var fs = require('fs');
   console.log(request.body);
   //console.log(req);
   //Use Formidable to parse the form
   form.parse(request, function(err, fields, files) {

    //TODO: find series
    var isProof = request.body.proof == 1 ? true : false;
    console.log(fields);
    console.log(fields.series);
    console.log(fields.proof);
    console.log(isProof);
    CoinSeries.findOne({$and: [{series: fields.series}, {proof: isProof}]}, function(err, series){
      if(err)
        response.status(500).json(err);
      else{
        console.log("******SERIES", series);
        //Setup new Coin
        var uploadedCoin = new Coins({

          //pcgs_number: request.body.pcgs_number,
          cert_number: this._id,
          year: fields.year,
          mintmark: fields.mintmark,
          date_mintmark: fields.year + "" +fields.mintmark,
          //denomination: {type: String},
          // country: request.body.country,
          // grade: request.body.grade,
          //image_url: {type: String},
          series: series,
          images: {
            "obverse": [],
            "reverse": [],
            "edge": []
          },
          uploadedBy: request.user
        });

         AWS.config.region = 'us-east-1';
      //


          var picTypeCounts = {
                     reverse: 0,
                     obverse: 0,
                     edge: 0
                   };

           var uploadFile = function(file, files){
             var fileToUpload = files[file];
             console.log(fileToUpload.path);
             var picType = file.indexOf("obverse") > -1 ? "obverse" : (file.indexOf("reverse") > -1 ? "reverse" : "edge");

             //Increment pic counts
             picTypeCounts[picType]++;


             var amazonKey = "user_coins/" + request.user._id + "/" + uploadedCoin._id + "/" +picType + "/" + fileToUpload.name;


             //Read the file from the temp location
             fs.readFile(fileToUpload.path, function (err,data) {
             //  console.log(data);
               if (err) {
                 return console.log(err);
               }

               //Push the new picture column
               //mutations.push(new HBaseTypes.Mutation({ column: "images:" + picType + picTypeCounts[picType], value: amazonKey }));


               //var s3bucket = new AWS.S3({params: {}});
               // s3bucket.createBucket(function() {
                 //var params = {};
                 console.log(fileToUpload);
                 console.log(data);
                 new AWS.S3().upload({Key: amazonKey,
                                     Bucket: 'numismastery',
                                     //'ContentLength': data.toString('binary').length,
                                     Body: data,
                                     ContentEncoding: "utf8",
                                     //'ContentType ': fileToUpload.type//,
                                     // Metadata: {
                                     //     'Content-Type ': fileToUpload.type
                                     // }
                                   }, function(err, data) {
                   if (err) {
                     //TODO: push any errors into a response
                     console.log("Error uploading data: ", err);
                   } else {
                     console.log(data);
                     //TODO: push successes into a response

                     console.log("Successfully uploaded data to " + amazonKey);
                   }
                 });
               // });

             });
             return amazonKey;
           };


           for(var file in files){

             var amazonKey = uploadFile(file, files);
             var picType = file.indexOf("obverse") > -1 ? "obverse" : (file.indexOf("reverse") > -1 ? "reverse" : "edge");
             uploadedCoin.images[picType].push({
               fileName: files[file].name,
               amazonKey: amazonKey
             });
             ///mutations.push(new HBaseTypes.Mutation({ column: "images:" + files[file].name, value: amazonKey }));

           }

           //Make the default image the first obverse photo
           uploadedCoin.image_url = "https://s3.amazonaws.com/numismastery/" + uploadedCoin.images.obverse[0].amazonKey;

           uploadedCoin.save(function(err, savedCoin){
             if(err) return res.status(500).json({status: "error", errors: [err]});
             //Decrement coin credits
             request.user.coinCredits--;
             request.user.save(function(err, savedUser){
               if(err) return res.status(500).json({status: "error", errors: [err]});
               response.json({coinId: savedCoin._id});
             });

           });




    }

  });
  });
});
};
