var express = require('express')
  , router = express.Router()
  , mongoose = require('mongoose'),
  // config = require('config'),
  session = require('express-session'),
  cookieParser = require('cookie-parser'),
  passport = require('passport');
  MongoStore = require('connect-mongo')(session);



module.exports = function(app) {
  var CoinGrades = mongoose.model('CoinGrades');
  var Coins = mongoose.model('Coins');
  var User = mongoose.model('User');
  var controller = require('./controllers/import.ctrl.js')(User);

  
  //TODO: ensure authenticated first

  app.post('/api/import/PCGS', controller.importFromPCGS);
  app.post('/api/import/NGC', controller.importFromNGC);
  app.post('/api/import/ANACS', controller.importFromANACS);


};
