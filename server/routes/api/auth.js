var express = require('express')
  , router = express.Router()
  , mongoose = require('mongoose'),
  // config = require('config'),
  session = require('express-session'),
  cookieParser = require('cookie-parser'),
  passport = require('passport');
  MongoStore = require('connect-mongo')(session);



module.exports = function(app) {
  var CoinGrades = mongoose.model('CoinGrades');
  var Coins = mongoose.model('Coins');
  var User = mongoose.model('User');
  var controller = require('./controllers/auth.ctrl.js')(User);
  console.log(controller);
  //var GradeScale = require('../models/GradeScale');

  app.get('/logout', controller.logout);

  app.post('/api/auth/signup', controller.signup);
  app.post('/api/auth/login', controller.login);

  app.post('/api/auth/forgotpassword', controller.forgotpassword);
  app.post('/api/auth/resetpassword', controller.resetpassword);
  app.post('/api/recaptcha/verify', controller.checkRecaptcha);


};
