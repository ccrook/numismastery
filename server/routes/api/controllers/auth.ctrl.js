
module.exports = function(User){
var config = require("../../../../config/config.js");
var https = require("https");
var bCrypt = require('bcrypt');
var passport = require('passport');


  function verifyPasswordRequirements(password, confirm, callback){
    var errors = [];

    var isCorrectLength = /^.{8,32}$/.test(password);
    var hasUpperCase = /[A-Z]/.test(password);
    var hasNumbers = /\d/.test(password);
    var hasNonalphas = /\W/.test(password);
    //TODO: confirm password complexity
    //Min 8 characters
    if(!isCorrectLength){
      errors.push("Password must be between 8 and 32 characters");
    }

    //1 non-alphanumeric
    if(!hasNonalphas){
      errors.push("Password must contain at least one non-alphanumeric character");
    }

    //1 capital
    if(!hasUpperCase){
      errors.push("Password must contain at least one uppercase letter");
    }

    if(!hasNumbers){
      errors.push("Password must contain at least one number");
    }

    //TODO: confirm passwords match
    if(password !== confirm){
      errors.push("Passwords must match");
    }

    if(callback && typeof(callback) == "function"){
      if(errors.length > 0) return callback(errors);
      return callback(null);
    }
  }

//TODO: pull this login out into functions + callbacks
  function duplicateDisplayNameCheck(displayName){
    //TODO: implement check against Mongo
  }

  function duplicateEmailAddressCheck(emailAddress){
    //TODO: check for existing accounts (strip crap between + and @ in case using aliases)
  }





    //,Recaptcha = require('recaptcha').Recaptcha;
    //TODO: pull into utility class
  function verifyRecaptcha(key, callback) {
    https.get("https://www.google.com/recaptcha/api/siteverify?secret=" + config.recaptcha.private_key + "&response=" + key, function(res) {
        var data = "";
        res.on('data', function (chunk) {
                        data += chunk.toString();
        });
        res.on('end', function() {
            try {
                var parsedData = JSON.parse(data);
                console.log(parsedData);
                callback(undefined, parsedData.success);
            } catch (e) {
                callback(e, false);
            }
        });
    });
  }

  function isLoggedIn(req, res, next) {

      // if user is authenticated in the session, carry on
      if (req.isAuthenticated())
          return next();

      // if they aren't redirect them to the home page
      res.redirect('/home');
  }

  function createHash(password){
    return bCrypt.hashSync(password,  bCrypt.genSaltSync(10));
  }

  var signup = function(req, res){
    var data = {
        remoteip:  req.connection.remoteAddress,
        secret: config.recaptcha.private_key,
        response:  req.body["g-recaptcha-response"]
    };

    console.log(req.body);
    var errors = [];

    //validate fields required to sign up
    verifyRecaptcha(data.response, function(err, response){
      console.log(err, response, errors.length);
      if(response){
        console.log("inside response true");
        if(errors.length <= 0){
          console.log("errors zero");

          //TODO: check for duplicate email addresses
          var emailStripped = req.body.email.replace(/(?=\+)(.*?)(?=@)/, '');
          var prefix = req.body.email.match(/.*?(?=[\+|@])/);
          var domain = req.body.email.match(/@(.*)/);

          console.log("EMAIL CHECKS");
          console.log(emailStripped);
          console.log(prefix[0], domain[0]);

          //Check if email address in use
          User.find({ $or: [{email: emailStripped},
                            {email: new RegExp('^' + prefix[0] + '\+.*?@' + domain[0])} ]}, function(err, userFound){
            if(err) return res.status(500).json(err);
            console.log("FOUND USER: ", userFound);
            if(userFound && userFound !== null && userFound.length > 0){
              return res.status(409).json({status: "error", errors: ["Account already exists for this email.  Please try again."]}); //conflict error code
            }

            //Check if emails match
            if(req.body.email !== req.body.confirm_email){
              errors.push("Email addresses must match.");
              return res.status(400).json({status: "error", errors: errors});
            }

            //error checking etc.
            verifyPasswordRequirements(req.body.password, req.body.confirm_password, function(err){
              if(err) return res.status(400).json({status: "error", errors: err});

              var newUser = new User({
                  email: req.body.email,
                  first_name: req.body.first_name,
                  last_name: req.body.last_name,
                  displayName: req.body.displayName,
                  provider: 'local'
                });

                console.log("PASS", req.body.password);
                newUser.password = createHash(req.body.password);

                newUser.generateConfirmationCode(function(err, user){
                  if(err) return res.status(500).json(err);

                  console.log(user);
                  User.register(user, function(err, user){
                    console.log("SAVING NEW USER");
                     console.log(err);
                     if(err && err !== null)
                       return res.status(400).json(err);

                     req.mailer.sendAccountConfirmation(user);
                     return res.status(200).json({
                       status: 'Registration successful!'
                     });
                  });
                  // return user.save(function(err, user){
                  //
                  //
                  // });
                });
            });


          });





        }else{
          return res.status(403).json(errors);
        }

      }
      else{
        return res.status(403).json(errors);
      }

    });

  };

  var login = function(req, res, next){
    passport.authenticate('local', function(err, user, info) {
     if (err) {
       return next(err);
     }
     if (!user) {
       return res.status(401).json({
         err: info
       });
     }
     req.logIn(user, function(err) {
       if (err) {
         return res.status(500).json({
           err: 'Could not log in user'
         });
       }
       res.status(200).json({
         status: 'Login successful!'
       });
     });
    })(req, res, next);
  };

  var logout = function(req, res, next){
      req.logout();
      res.redirect('/login');
  };



  var forgotpassword = function(req, res, next){
    var email = req.body.email;
    console.log(email);
    User.findOne({email: email, password: { $exists: true }}, function(err, user){
      if(err)
        res.status(500).json({error: err});
        console.log("FOUND USER: ", user);
      if(user && user !== null){

        user.generateResetToken(function(err, user){
          console.log("NEW TOKEN",user);
          req.mailer.sendPasswordReset(user);
        });

      }
      res.status(200).json({status: "Email sent to " + email + " containing password reset instructions."});
    });
  };

  var resetpassword = function(req, res, next){
    var token = req.body.token;
    var password = req.body.password;
    var confirm = req.body.confirm;
    console.log(req.body);
    //TODO: confirm token is legit
    if(password !== confirm){
      res.status(400).json({error: "Passwords must match."});
    }
    else{
      User.findOne({reset_token: token}, function(err, user){
        if(err)
          return res.status(500).json({error: err});
        console.log(password);
        user.setPassword(password, function(err){
          //Clear the reset token
          user.reset_token = undefined;
          user.save(function(err){
            if(err)
              return res.status(500).json({error: err});

            //TODO: actually send this as an email
            req.mailer.test();
            res.status(200).json({status: "Successfully reset password."});
          });
        });

      });
    }

  };

  var checkRecaptcha = function(req, res, next){
    if(req.user){
      verifyRecaptcha(req.body["g-recaptcha-response"], function(err, response){
        console.log(err, response);
        if(err) return res.status(400).json(err);

        if(response === true){
          req.user.lastRecaptcha = Date.now();
          req.user.save(function(err, user){
            return res.status(200).json(true);
          });

        }else{
          return res.status(400).send();
        }



      });
    }

  };


  return {
    signup: signup,
    login: login,
    logout: logout,
    verifyRecaptcha: verifyRecaptcha,
    forgotpassword: forgotpassword,
    resetpassword: resetpassword,
    checkRecaptcha: checkRecaptcha
  };
};
