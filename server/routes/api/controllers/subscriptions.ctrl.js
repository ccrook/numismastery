

module.exports = function(User, paypal){
  var billingPlans = require('../../../../config/billingPlans')();


  //TODO: get Billing Plan IDs and see if any need to be created (i.e. during test)


  //TODO: create billing Plans

  //TODO: create billing agreements

  var get = function(req, res){
    console.log(billingPlans);
    var url = require('url');
    //var config = require('../../../../config/config');
    //console.log(config.paypal.api_config);

    paypal.billingPlan.create(billingPlans.copperBillingPlan, function(error, billingPlan){
      if (error) {
          throw error;
      } else {
        //TODO: stash this in mongo
          // paypal.billingPlan.activate(billingPlan.id, function(error, activatedResponse){
          //   console.log(activatedResponse);
          // })
          var isoDate = new Date();
          isoDate.setSeconds(isoDate.getSeconds() + 5);
          isoDate = isoDate.toISOString().slice(0, 19).toString() + 'Z';


          var billingPlanUpdateAttributes = billingPlans.activateBillingPlan;
          var billingAgreementAttributes = billingPlans.copperBillingAgreement(isoDate);

          paypal.billingPlan.update(billingPlan.id, billingPlanUpdateAttributes, function (error, response) {
            if (error) {
                console.log(error);
                throw error;
            } else {
                console.log("Billing Plan state changed to " + billingPlan.state);
                billingAgreementAttributes.plan.id = billingPlan.id;

                // Use activated billing plan to create agreement
                paypal.billingAgreement.create(billingAgreementAttributes, function (error, billingAgreement) {
                    if (error) {
                        console.log(error);
                        throw error;
                    } else {
                        console.log("Create Billing Agreement Response");
                        //console.log(billingAgreement);
                        for (var index = 0; index < billingAgreement.links.length; index++) {
                            if (billingAgreement.links[index].rel === 'approval_url') {
                                var approval_url = billingAgreement.links[index].href;
                                console.log("For approving subscription via Paypal, first redirect user to");
                                console.log(approval_url);

                                console.log("Payment token is");
                                console.log(url.parse(approval_url, true).query.token);
                                // See billing_agreements/execute.js to see example for executing agreement
                                // after you have payment token
                            }
                        }
                    }
                });
            }
        });
      }
    });


      var list_billing_plan = {
      'status': 'ACTIVE',
      'page_size': 5,
      'page': 1,
      'total_required': 'yes'
    };

    paypal.billingPlan.list(list_billing_plan, function (error, billingPlan) {
      if (error) {
          throw error;
      } else {
          console.log("List Billing Plans Response");
          console.log(billingPlan);
          if(billingPlan){
            console.log(billingPlan);
          }
      }
    });
  };

  var cancel = function(req, res){


      throw "Not yet implemented.";

    //TODO: return success/fail of results
  };

  var suspend = function(req, res){


      throw "Not yet implemented.";

    //TODO: return success/fail of results
  };

  var enroll = function(req, res){


      throw "Not yet implemented.";

    //TODO: return success/fail of results
  };


  return {
    get: get,
    cancel: cancel,
    suspend: suspend,
    enroll: enroll
  };
};
