
module.exports = function(User){
var config = require("../../../../config/config.js");
var https = require("https");
var http = require("http");
var bCrypt = require('bcrypt');
var passport = require('passport');
var request = require('request');
var cheerio = require('cheerio');
    //,Recaptcha = require('recaptcha').Recaptcha;
  //TODO: move this to a utility class
  function verifyRecaptcha(key, callback) {
    https.get("https://www.google.com/recaptcha/api/siteverify?secret=" + config.recaptcha.private_key + "&response=" + key, function(res) {
        var data = "";
        res.on('data', function (chunk) {
                        data += chunk.toString();
        });
        res.on('end', function() {
            try {
                var parsedData = JSON.parse(data);
                console.log(parsedData);
                callback(undefined, parsedData.success);
            } catch (e) {
                callback(e, false);
            }
        });
    });
  }


    var importFromPCGS = function(req, res){
      var certNumbers = req.body.certNumbers;
      if(certNumbers.length > 10) return res.status(400).json("Maximum 10 certification numbers per request.  Please try again.");


      //TODO: parse out cert numbers passed in
    //
    //   var startingCertNumber = 25650000;
    //   var endingCertNumber = 25850000;
    //   var filename = 'certs' + '_' + startingCertNumber + '_' + endingCertNumber + '.json';
      var baseUrl = "http://www.pcgs.com/cert/";
      var results = {};
      var pool = new http.Agent(); //Your pool/agent
      //  pool.maxSockets = 2;

      var options = {

        baseUrl: baseUrl,
        pool: pool,
        headers: {
          'User-Agent': "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36"
        }
      };

      function pcgsResponseHandler(error, response, html){
        var results = {};

        if(!error && response.statusCode == 200){

          //TODO: additional check we're on the right page and the cert number exists

          var $ = cheerio.load(html);

          console.log(response.req.path.replace("/cert/", ""));
          var json = {
            pcgs_number : "",
            cert_number : response.req.path.replace("/cert/", ""),
            date_mintmark : "",
            denomination : "",
            country: "",
            grade: "",
            image_url: "",
            detail_url: ""
          };


          json.date_mintmark = $("#YearIssued").text().trim();
          json.pcgs_number = $("#lblPcgsNo").text().trim();
          json.denomination = $("#Denomination").text().trim();
          json.country = $("#Country").text().trim();
          json.grade = $("#Grade").text().trim();
          json.detail_url= $("#cert-verification-app").children("a:contains('View this coin in PCGS CoinFacts')");

          //TODO: go to detail url to get series information

        	if(json.country.indexOf("United States") > -1 )
        	{
                $("div.coin-image img").filter(function(){
                  var data = $(this);

                  json.image_url = data.attr("src");
                  if(json.image_url.indexOf("noimg") > -1){
                    json.noTpgImages = true;
                  }else{
                    json.noTpgImages = false;
                  }
                });

              if(json.image_url.indexOf("Thumb") > -1){

                fs.appendFile(filename, JSON.stringify(json, null, null) + "\n", function(err){

                    console.log('Appended.');
                });
              }
            }
            else{
              console.log("Bummer: " + error);
            }
	        }

          //TODO: download images from PCGS and upload to S3 (public ACL)
          //TODO: write this to the database?
      }

      for(var currentCert = 0; currentCert < certNumbers.length; currentCert++){

        options.uri = certNumbers[currentCert] + "";

        request.get(options, pcgsResponseHandler);
      }

    //TODO: return success/fail of results
  };


    var importFromNGC = function(req, res){

      //TODO: end point to hit: https://www.ngccoin.com/certlookup/1850079-001/
      //TODO: without images: https://www.ngccoin.com/certlookup/695456-004/
      //TODO: Information to parse

  //     <div class="cert-details">
  // 				<strong>Date/Info:</strong> 1919 S   <br>
  // 				<strong>Denomination:</strong> 50C<br>
  // 				<strong>Grade:</strong> DELETED <br>
  // 													<strong>NGC Price Guide:</strong>
  // N/A					<br>
  // 				<strong>NGC Census:</strong>
  // N/A				<br>
  // 					<strong>NGC Coin Explorer:</strong>
  // 						N/A<br>
  // 					<br>
  // 				<br>
  // 				<div class="disclaimer-text">
  // 					If the information displayed above is incorrect or does not match the coin you are verifying, or if you believe that you have a counterfeit or tampered NGC holder, please contact <a href="mailto:consumerawareness@ngccoin.com">ConsumerAwareness@ngccoin.com</a>. To learn more about counterfeit or tampered NGC holders, <a href="http://www.ngccoin.com/coin-grading/counterfeit-advisory/">click here</a>.
  // 				</div><br>
  // 					<a ng-click="showDisclaimer = true">NGC Price Guide Disclaimer</a>
  //
  // 		</div>

  //TODO: Images
  // <div class="cert-images">
  // 					<div class="fancybox">
  // 						<a title="Obverse" href="http://s3.amazonaws.com:80/ngc-images/33cb58f6-7e48-46a3-8e28-377c410d30cf_obv.jpg" ng-click="events.showFullImage($event)">
  // 							<img src="http://s3.amazonaws.com:80/ngc-images/33cb58f6-7e48-46a3-8e28-377c410d30cf_obv_t.jpg">
  // 						</a>
  // 					</div>
  // 					<div class="fancybox">
  // 						<a title="Reverse" href="http://s3.amazonaws.com:80/ngc-images/33cb58f6-7e48-46a3-8e28-377c410d30cf_rev.jpg" ng-click="events.showFullImage($event)">
  // 							<img src="http://s3.amazonaws.com:80/ngc-images/33cb58f6-7e48-46a3-8e28-377c410d30cf_rev_t.jpg">
  // 						</a>
  // 					</div>
  // 				</div>

      throw "Not yet implemented.";
    };

    function importFromANACS(req, res){

    }

    return {
      importFromPCGS: importFromPCGS,
      importFromNGC: importFromNGC,
      importFromANACS: importFromANACS
    };


};
