module.exports = function(User){

//TODO: move to util
  var extractParams = function(req){
    console.log(req.param);
    var perPage = parseInt(req.param('perPage')) || 10;
    var page = Math.max(1, (parseInt(req.param('page')) || 1));


    return {
      page: page,
      perPage: perPage
    };
  };

  var comments = function(req, res){
    if(req.user && req.user !== null){

      var params = extractParams(req);

      req.user.getComments(params, function(err, coins){
        if(err){
          res.status(500).json(err);
        }
        res.json(coins);
      });
    }else{
      res.status(401).send("Access denied.  You must be logged in.");
    }
  };

  var gradedCoins = function(req, res){
    if(req.user && req.user !== null){

      var params = extractParams(req);

      req.user.getGradedCoins(params, function(err, coins){
        if(err){
          res.status(500).json(err);
        }
        res.json(coins);
      });
    }else{
      res.status(401).send("Access denied.  You must be logged in.");
    }
  };

  var uploadedCoins = function(req, res){
    if(req.user && req.user !== null){

      var params = extractParams(req);

      req.user.getUploadedCoins(params, function(err, coins){
        if(err){
          return res.status(500).json(err);
        }
        res.json(coins);
      });
    }else{
      res.status(401).send("Access denied.  You must be logged in."); //TODO: break this out into its own module for reuse
    }

  };

  var status = function(req,res,next){
    if (!req.isAuthenticated()) {
      return res.status(200).json({
        status: false
      });
    }
    res.status(200).json({
      status: true
    });
  };

  //Strip out password field
  function filterUser(user){
    //var userCopy = JSON.parse(JSON.stringify(user));
    user.password = undefined;
    return user;
  }

  var get = function(req,res,next){
    console.log(req.user);
    if(req.user){
      return res.json(filterUser(req.user));
    }
    return res.send();
  };

  var getByToken = function(req,res,next){
    User.getByToken(req.params.token, function(err, user){
      if(err)
        return res.status(500).json(err);
      res.json(user.email);
    });
  };

  var getByConfirmationCode = function(req,res,next){
    User.getByConfirmationCode(req.params.token, function(err, user){
      if(err)
        return res.status(500).json(err);
      if(user === null)
        return res.redirect("/error");
      user.accountConfirmation.dateConfirmed = Date.now();
      //Flush confirmation code
      user.accountConfirmation.code = undefined;
      user.save(function(err, user){
        if(err)
          return res.status(500).json(err);
        req.mailer.sendWelcome(user);
        res.redirect("/login?confirmed=true");
      });

    });
  };

  var enable = function(req,res,next){
    req.user.enable(function(err, response){
      if(err) res.status(500).json(err);
      res.json(response);
    });
  };

  var disable = function(req,res,next){
    req.user.disable(function(err, response){
      if(err) res.status(500).json(err);
      res.json(response);
    });
  };

  var acceptTerms = function(req,res,next){
    req.user.acceptTerms(function(err, response){
      if(err) res.status(500).json(err);

      res.json(response);
    });
  };

  var displayNameTaken = function(req,res,next){
    var displayName = req.params.displayname;
    console.log("TAKEN :", req.params.displayname);
    User.findOne({displayName: displayName}, function(err, user){
      console.log("DISPLAY NAME TAKEN", user, err);
      if(err) return res.status(500).json(err);
      if(user === null)
        return res.status(200).json({taken: false});
      return res.status(200).json({taken: true});
    });
  };

  function handleErrors(err, user, callback){
    //TODO: implement standard error handling, in separate module
  }

  function giveCredits(req, res, next){
    var userId = req.body.id;
    var creditsToGive = req.body.credits;

    function handleCreditsGiven(err, user){
      if(err) return res.status(500).json(err);
      if(user === null) return res.status(400).json(badRequest);
      res.status(200).json(filterUser(user));
    }

    function userLookupCallback(err, user){
      if(err) return res.status(500).json(err);
      if(user === null) return res.status(404).json({status: "User not found."});
      user.giveCredits(creditsToGive, handleCreditsGiven);
    }

    User.findOne({_id: id}, userLookupCallback);

  }

  return {
    comments: comments,
    gradedCoins: gradedCoins,
    uploadedCoins: uploadedCoins,
    status: status,
    get: get,
    getByToken: getByToken,
    getByConfirmationCode: getByConfirmationCode,
    enable: enable,
    disable: disable,
    acceptTerms: acceptTerms,
    displayNameTaken: displayNameTaken,
    giveCredits: giveCredits
  };
};
