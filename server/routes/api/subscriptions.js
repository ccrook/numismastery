var express = require('express')
  , router = express.Router()
  , mongoose = require('mongoose'),
  // config = require('config'),
  session = require('express-session'),
  cookieParser = require('cookie-parser'),
  MongoStore = require('connect-mongo')(session);



module.exports = function(app, config) {
  var CoinGrades = mongoose.model('CoinGrades');
  var Coins = mongoose.model('Coins');
  var User = mongoose.model('User');

  var paypal = require('../../../config/paypal')(config);
  paypal.configure(config.paypal.api_config);


  var controller = require('./controllers/subscriptions.ctrl.js')(User, paypal);



  console.log(controller);
  //var GradeScale = require('../models/GradeScale');

  app.get('/api/subscription', controller.get);
  app.post('/api/subscription/enroll', controller.enroll);
  app.post('/api/subscription/suspend', controller.suspend);
  app.post('/api/subscription/cancel', controller.cancel);




};
