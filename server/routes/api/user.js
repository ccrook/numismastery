var express = require('express')
  , router = express.Router()
  , mongoose = require('mongoose'),
  // config = require('config'),
  session = require('express-session'),
  cookieParser = require('cookie-parser'),
  MongoStore = require('connect-mongo')(session);


function isAuthenticated(req, res, next) {

    // do any checks you want to in here

    // CHECK THE USER STORED IN SESSION FOR A CUSTOM VARIABLE
    // you can do this however you want with whatever variables you set up
    if (req.user.authenticated)
        return next();

    // IF A USER ISN'T LOGGED IN, THEN REDIRECT THEM SOMEWHERE
    res.redirect('/');
}

module.exports = function(app) {

  console.log(controller);
  //var GradeScale = require('../models/GradeScale');
  var CoinGrades = mongoose.model('CoinGrades');
  var Coins = mongoose.model('Coins');
  var User = mongoose.model('User');

  var controller = require('./controllers/user.ctrl.js')(User);

  app.get('/api/user/coins/graded', controller.gradedCoins);
  app.get('/api/user/coins/uploaded', controller.uploadedCoins);
  app.get('/api/user/comments', controller.comments);
  app.get('/api/user/status', controller.status);
  app.get('/api/user', controller.get);
  app.get('/api/user/token/:token', controller.getByToken);
  app.get('/api/user/confirmation/:token', controller.getByConfirmationCode);
  app.get('/api/user/displayname/:displayname', controller.displayNameTaken);
  app.post('/api/user/:id/enable', controller.enable);
  app.post('/api/user/:id/disable', controller.disable);
  app.post('/api/user/:id/acceptterms', controller.acceptTerms);



  app.get('/api/sendgrid', function(req, res){
    app.mailer.test();
    // var mailer = require('../../services/mailer');
    //
    // mailer.test();
    res.end();
  });

};
