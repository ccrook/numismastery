var express = require('express');
var passport = require("passport");
var bCrypt = require('bcrypt');
var User = require('mongoose').model('User');

module.exports = function(app) {
  app.get('/auth/facebook', passport.authenticate('facebook', {
    failureRedirect: '/login'
  }));

  app.get('/auth/facebook/callback', passport.authenticate('facebook', {
    scope: 'email',
    failureRedirect: '/login',
    successRedirect: '/home'
  }));

  app.get('/auth/google', function(req,res,next){
    console.log("SET SESSION", req.params, req.query);
    if(req.query.redirectUrl){
      req.session.redirectUrl = req.query.redirectUrl;
    }
    next();
  }, passport.authenticate('google', {
    scope: 'https://www.googleapis.com/auth/userinfo.email'
  }));

  app.get('/auth/google/callback', function(req, res, next){

  passport.authenticate('google', function(err, user, info){
    console.log("GOOGLE AUTH ERROR", err);
    // failureRedirect: '/login',
    // successRedirect: '/home'
    //From" http://stackoverflow.com/questions/9885711/custom-returnurl-on-node-js-passports-google-strategy
    var redirectUrl = '/home';

    if (err) { return next(err); }
    if (!user) { return res.redirect('/login'); }
    console.log("SESSION**", req.session.redirectUrl);
    if (req.session.redirectUrl) {
      redirectUrl = req.session.redirectUrl;
      req.session.redirectUrl = null;
    }
    req.logIn(user, function(err){
      if (err) { return next(err); }
    });
    res.redirect(redirectUrl);
  })(req,res,next);
});


//TODO: get rid of this
  app.post('/user', function(req, res) {
    res.json(req.user);
  });
  // route middleware to make sure a user is logged in
  function isLoggedIn(req, res, next) {

      // if user is authenticated in the session, carry on
      if (req.isAuthenticated())
          return next();

      // if they aren't redirect them to the home page
      res.redirect('/');
  }
  var createHash = function(password){
    //return "derp";
     return bCrypt.genSaltSync(10, function(salt){
       return bCrypt.hashSync(password, salt);
     });
  };

  var isValidPassword = function(user, password){
    return bCrypt.compareSync(password, user.password);
  };



  app.post('/auth/setusername', function(req, res){
    var newUsername = req.body.username;
    console.log(newUsername);
    console.log(req.user);
    //TODO: check if already in use
    User.findOne({"displayName": newUsername}).exec(function(err, checkExists){
      console.log("FIND ONE CALLBACK");
      console.log(err);
      if(err !== null){
        return res.json(err);
      }else{

        console.log(checkExists);
        if(checkExists !== null){
          //return an error response code
          res.status(400); //bad request
          res.json([
            "Username " + newUsername + " is already in use.  Please try again with a different username."
          ]);
          return res.end();
        }
        User.findOne({"email": req.user.email}).exec(function(err, userDoc){
          if(err){
            return res.json(err);
          }

          else {
            userDoc.displayName = newUsername;
            userDoc.save(function(err, user){
            if(err) return err;
              return res.json(userDoc);
            });
          }
        });
      }
    });

  });
};
