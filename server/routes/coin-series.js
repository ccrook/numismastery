var express = require('express')
  , router = express.Router()
  , mongoose = require('mongoose');
  // config = require('config'),
  // session = require('express-session'),
  // cookieParser = require('cookie-parser'),
  // MongoStore = require('connect-mongo')(session);

module.exports = function(app) {
  //TODO: pull from config
  // var mongoDatabase = "numismastery";
  // var mongoPass = "derp123!!";
  // //DB setup
  // var connectionString = "mongodb://prod:" + mongoPass + "@c350.candidate.57.mongolayer.com:10350/numismastery";
  //
  // mongoose.connect(connectionString);
  var db = mongoose.connection;
  // db.on("error", console.error.bind(console, "connection error..."));
  // db.once("open", function callback(){
  //   console.log("numismastery mongo db opened");
  // });

  var CoinSeries = require('../models/CoinSeries.js');

  app.get('/coin-series', function(req, res){
    //TODO: pull from Hbase instead
    console.log("COIN SERIES");
    //
    CoinSeries.aggregate(
      [ {$project: { _id: 0, series: 1, pcgs_numbers: 1, proof: {$cond: { if: { $eq: [ "$proof", true ] }, then: 1, else: 0}} } },
        {$unwind: "$pcgs_numbers" },
        {$group: { "_id" : "$series", pcgs_numbers: { $push: "$pcgs_numbers" }, proof: {$avg: "$proof" } } },
        //{$project: {_id: 0, series: 1, pcgs_number: 1, proof: {$cond: { if: {proof: {$eq: 1}}, then: true, else: false }}}}
      ]).exec(function(err, coinSeries){//({}, function (err, coinSeries) {
      console.log("COIN SERIES ERROR", err);
      //console.log(coinSeries);

      if(err){
        res.send(err);
      }
      res.send(coinSeries);
    });

  });
};
// module.exports = function(app) {
//   app.get('/coin-series', function(request, response) {
//     response.send('derp');
//   });
// };
