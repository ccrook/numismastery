var gradeCache = [];
var express = require('express')
  , router = express.Router()
  , mongoose = require('mongoose'),
  // config = require('config'),
  session = require('express-session'),
  cookieParser = require('cookie-parser'),
  MongoStore = require('connect-mongo')(session);

module.exports = function(app) {

  var GradeScale = require('../models/GradeScale');

  app.get('/grades', function(req, res){
    GradeScale.find({}, function(err, grades){
      if(err){
        res.json(err);
      }
      res.json(grades);
    });
  });

};
